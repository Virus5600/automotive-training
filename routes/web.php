<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// AJAX FUNCTIONS
Route::get('/ajax/get/cities', 'AddressController@getCities')->name('ajax.getCities');
Route::get('/ajax/get/productType', 'AjaxController@getProductType')->name('ajax.getProductType');
Route::get('/ajax/get/productComponent', 'AjaxController@getProductComponent')->name('ajax.getProductComponent');

// USER PAGE
Route::get('/' , 'UserSideController@home')->name('home');
Route::get('/products' , 'UserSideController@products')->name('products');
Route::get('/about_us' , 'UserSideController@about_us')->name('about_us');
Route::get('/payment_methods' , 'UserSideController@payment_methods')->name('payment_methods');
Route::get('/wishlist' , 'UserSideController@wishlist')->name('wishlist');

// CONTACT US
Route::get('/contact_us' , 'UserSideController@contact_us')->name('contact_us');
Route::post('/create/concern' , 'ContactsController@storeConcern')->name('storeConcern');


// AUTHENTICATE
Route::get('/login' , 'UserSideController@login')->name('login');
Route::get('/register' , 'UserSideController@register')->name('register');
Route::post('/authenticate', 'UserSideController@authenticate')->name('authenticate');
Route::get('/logout', 'UserSideController@logout')->name('logout');
Route::get('/reset_password', 'UserSideController@reset_password')->name('reset_password');

Route::post('/register/account' , 'UserSideController@store')->name('userside.store');


// MY ACCOUNT
Route::get('/profile/view' , 'UserSideController@profile')->name('profile');
Route::get('/profile/edit' , 'UserSideController@edit_profile')->name('edit_profile');
Route::get('/wishlist' , 'UserSideController@wishlist')->name('wishlist');
Route::get('/address_checkout' , 'UserSideController@address_checkout')->name('address_checkout');

Route::post('/profile/edit/account' , 'UserSideController@update')->name('userside.update');
Route::post('/profile/edit/password' , 'UserSideController@change_password')->name('change_password');


// CANVASSING
Route::group(['prefix' => 'canvass'], function() {
    Route::get('/index' , 'CanvassController@index')->name('canvass');
    Route::get('/create' , 'CanvassController@create')->name('canvass_create');
    Route::get('/{id}/view' , 'CanvassController@view')->name('canvass_view');

    Route::post('/store/canvass' , 'CanvassController@storeCanvass')->name('storeCanvass');
});

//APPOINTMENT
Route::group(['prefix' => 'appointment'], function() {
    Route::get('/index' , 'AppointmentController@index')->name('appointment');
    Route::get('/create' , 'AppointmentController@create')->name('appointment_create');
    Route::get('/{id}/view' , 'AppointmentController@view')->name('appointment_view');

    Route::post('/store/appointment' , 'AppointmentController@storeAppointment')->name('storeAppointment');

});

//ADDRESS
Route::group(['prefix' => 'address'], function() {
    Route::get('/index' , 'AddressController@index')->name('address');
    Route::get('/create' , 'AddressController@create')->name('address_create');
    Route::get('/{id}/view' , 'AddressController@view')->name('address_view');
    Route::get('/{id}/edit' , 'AddressController@edit')->name('address_edit');

    Route::get('/{id}/create/address' , 'AddressController@createAddress')->name('createAddress');
    Route::post('/store/address' , 'AddressController@storeAddress')->name('storeAddress');
    Route::get('/{id}/edit/address' , 'AddressController@editAddress')->name('editAddress');
    Route::post('/{id}/update/address' , 'AddressController@updateAddress')->name('updateAddress');
    Route::get('/{id}/delete/address' , 'AddressController@deleteAddress')->name('deleteAddress');
	Route::get('/{id}/set-active','AddressController@activeAddress')->name('activeAddress');

});

//SETTINGS
Route::group(['prefix' => 'settings'], function() {
    Route::get('/edit_profile' , 'UserSideController@settings_profile')->name('settings_profile');
    Route::get('/change_password' , 'UserSideController@settings_password')->name('settings_password');
    Route::get('/change_email' , 'UserSideController@settings_email')->name('settings_email');
});

//CHECKOUT
Route::group(['prefix' => 'chekout'], function() {
    Route::get('/index' , 'CheckoutController@index')->name('checkout');
    Route::get('/view' , 'CheckoutController@view')->name('checkout_view');
    Route::get('/pay' , 'CheckoutController@pay')->name('checkout_pay');
});

//PRODUCTS-RELATED
Route::get('/order/myorder' , 'UserSideController@myorder')->name('myorder');
Route::get('/order/track_order' , 'UserSideController@track_order')->name('track_order');
Route::get('/cart' , 'UserSideController@mycart')->name('mycart');
Route::get('/{id}/products/details' , 'UserSideController@details')->name('details');




//ADMIN SIDE
Route::group(['prefix' => 'admin'], function() {
    Route::group(['prefix' => 'dashboard'], function() {
        Route::get('/allbranch' , 'AdminController@allbranch')->name('allbranch');
        Route::get('/branch1' , 'AdminController@branch1')->name('branch1');
        Route::get('/branch2' , 'AdminController@branch2')->name('branch2');
    });

    Route::group(['prefix' => 'appointment'], function() {
        Route::get('/index' , 'AppointmentController@indexAdmin')->name('appointment.index');
        Route::get('/{id}/view' , 'AppointmentController@viewAdmin')->name('appointment.view');
        Route::get('/{id}/edit' , 'AppointmentController@editAdmin')->name('appointment.edit');
        Route::post('/{id}/update' , 'AppointmentController@updateAdmin')->name('appointment.update');
    });

    Route::group(['prefix' => 'canvass'], function() {
        Route::get('/index' , 'CanvassController@indexAdmin')->name('canvass.index');
        Route::get('/{id}/view' , 'CanvassController@viewAdmin')->name('canvass.view');
        Route::get('/{id}/edit' , 'CanvassController@editAdmin')->name('canvass.edit');
        Route::post('/{id}/update' , 'CanvassController@updateAdmin')->name('canvass.update');
    });

    Route::group(['prefix' => 'reports'], function() {
        Route::get('/index' , 'ContactsController@index')->name('reports.index');
        Route::get('/{id}/view' , 'ContactsController@view')->name('reports.view');
        Route::get('/{id}/edit' , 'ContactsController@edit')->name('reports.edit');
        Route::post('/{id}/update' , 'ContactsController@update')->name('reports.update');
    });

    Route::group(['prefix' => 'sales-order'], function() {
        Route::get('/index' , 'SalesOrderController@index')->name('sales.index');
    });

    Route::group(['prefix' => 'settings'], function() {
        Route::get('/index' , 'SettingsController@index')->name('settings.index');
        Route::post('/update' , 'SettingsController@update')->name('settings.update');
    });

    Route::group(['prefix' => 'store'], function() {
        Route::get('/index' , 'StoreController@index')->name('store.index');
        Route::get('/{id}/show' , 'StoreController@show')->name('store.show');
        Route::get('/create' , 'StoreController@create')->name('store.create');
        Route::post('/store' , 'StoreController@store')->name('store.store');
        Route::get('/{id}/edit' , 'StoreController@edit')->name('store.edit');
        Route::post('/{id}/update' , 'StoreController@update')->name('store.update');
        Route::get('/{id}/delete' , 'StoreController@delete')->name('store.delete');
    });

    Route::group(['prefix' => 'user'], function() {
        Route::get('/index' , 'UserAdminController@index')->name('user.index');
        Route::get('/{id}/show' , 'UserAdminController@show')->name('user.show');
        Route::get('/create' , 'UserAdminController@create')->name('user.create');
        Route::post('/store' , 'UserAdminController@store')->name('user.store');
        Route::get('/{id}/edit' , 'UserAdminController@edit')->name('user.edit');
        Route::post('/{id}/update' , 'UserAdminController@update')->name('user.update');
        Route::get('/{id}/edit-password' , 'UserAdminController@changePassword')->name('user.change-password');
        Route::post('/{id}/update-password' , 'UserAdminController@updatePassword')->name('user.update-password');
        Route::get('/{id}/delete' , 'UserAdminController@delete')->name('user.delete');

        Route::get('/{id}/create/address' , 'UserAdminController@createAddress')->name('user.createAddress');
        Route::post('/store/address' , 'UserAdminController@storeAddress')->name('user.storeAddress');
        Route::get('/{id}/edit/address/{user_id}' , 'UserAdminController@editAddress')->name('user.editAddress');
        Route::post('/{id}/update/address/{user_id}' , 'UserAdminController@updateAddress')->name('user.updateAddress');
        Route::get('/{id}/delete/address/{user_id}' , 'UserAdminController@deleteAddress')->name('user.deleteAddress');

    });

    Route::group(['prefix' => 'others'], function() {

        Route::get('/index' , 'OthersController@index')->name('others.index');

        Route::group(['prefix' => 'car'], function() {
            Route::get('/index' , 'OthersController@carindex')->name('car.index');
            Route::get('/{id}/show' , 'OthersController@carshow')->name('car.show');
            Route::get('/{id}/edit' , 'OthersController@caredit')->name('car.edit');
            Route::post('/{id}/update' , 'OthersController@carupdate')->name('car.update');
            Route::get('/create' , 'OthersController@carcreate')->name('car.create');
            Route::post('/store' , 'OthersController@carstore')->name('car.store');
            Route::get('/{id}/delete' , 'OthersController@cardelete')->name('car.delete');
        });

        Route::group(['prefix' => 'product'], function() {
            Route::group(['prefix' => 'brand'], function() {
                Route::get('/index' , 'OthersController@productBrand_index')->name('pBrand.index');
                Route::get('/create' , 'OthersController@productBrand_create')->name('pBrand.create');
                Route::post('/store' , 'OthersController@productBrand_store')->name('pBrand.store');
                Route::get('/{id}/delete' , 'OthersController@productBrand_delete')->name('pBrand.delete');
            });

            Route::group(['prefix' => 'type'], function() {
                Route::get('/index' , 'OthersController@productType_index')->name('pType.index');
                Route::get('/{id}/edit' , 'OthersController@productType_edit')->name('pType.edit');
                Route::post('/{id}/update' , 'OthersController@productType_update')->name('pType.update');
                Route::get('/create' , 'OthersController@productType_create')->name('pType.create');
                Route::post('/store' , 'OthersController@productType_store')->name('pType.store');
                Route::get('/{id}/delete' , 'OthersController@productType_delete')->name('pType.delete');
            });

            Route::group(['prefix' => 'payment_method'], function() {
                Route::get('/index' , 'OthersController@payment_index')->name('payment.index');
                Route::get('/{id}/show' , 'OthersController@payment_show')->name('payment.show');
                Route::get('/{id}/edit' , 'OthersController@payment_edit')->name('payment.edit');
                Route::post('/{id}/update' , 'OthersController@payment_update')->name('payment.update');
                Route::get('/create' , 'OthersController@payment_create')->name('payment.create');
                Route::post('/store' , 'OthersController@payment_store')->name('payment.store');
                Route::get('/{id}/delete' , 'OthersController@payment_delete')->name('payment.delete');
            });

            Route::group(['prefix' => 'component'], function() {
                Route::get('/index' , 'OthersController@component_index')->name('component.index');
                Route::get('/{id}/edit' , 'OthersController@component_edit')->name('component.edit');
                Route::post('/{id}/update' , 'OthersController@component_update')->name('component.update');
                Route::get('/create' , 'OthersController@component_create')->name('component.create');
                Route::post('/store' , 'OthersController@component_store')->name('component.store');
                Route::get('/{id}/delete' , 'OthersController@component_delete')->name('component.delete');
            });

        });

    });
});