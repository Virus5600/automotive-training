@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/payment.css">
@endsection

@section('title', 'Payment Methods')

@section('content')
    <h1 class="page-title">PAYMENT METHOD</h1>

    <div class="container-fluid justify-content-center align-items-center d-block method">

        @foreach ($payment as $p)
            <div class="container-fluid d-flex py-4 my-5 card">
                <div class="col-lg-12">
                    <h4>{{$p->method}}</h4>
                </div>
                <div class="col-lg-12 text-center py-5 cardinfo">
                    <h2>{{$p->info}}</h2>
                </div>
                <div class="col-lg-12 d-flex mx-auto row">
                    <div class="col-lg-8 col-md-8 col-sm-12 my-auto cardname">
                        <h4>{{$p->name}}</h4>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 paymentLogo"
                        style="background-image: url({{ asset('/uploads/method/' . $p->logo)}});">
                    </div>
                </div>
            </div>
        @endforeach

    </div>
@endsection
