<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- jQuery 3.6.0 -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- jQuery UI 1.12.1 -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Removes the code that shows up when script is disabled/not allowed/blocked -->
    <script type="text/javascript" id="for-js-disabled-js">
        $('head').append('<style id="for-js-disabled">#js-disabled { display: none; }</style>');
        $(document).ready(function() {
            $('#js-disabled').remove();
            $('#for-js-disabled').remove();
            $('#for-js-disabled-js').remove();
        });
    </script>

    <!-- popper.js 1.16.0 -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>

    <!-- Bootstrap 4.4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

    <!-- Slick Carousel 1.9.0 -->
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>

    <!-- Sweet Alert 2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <!-- Input Mask 5.0.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.5/jquery.inputmask.min.js"></script>

    <!-- Bootstrap 4 Select -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <!-- FontAwesome -->
    <script src="https://kit.fontawesome.com/f67ab1f0a2.js" crossorigin="anonymous"></script>

    <!-- Default CSS Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}">

    <link rel="icon"
        href="{{ asset('/uploads/settings/' . \App\Settings::where('settings', '=', 'logo')->first()->value) }}">

    @yield('srcs')

    <title>JAZZ | @yield('title')</title>
</head>

<body>
    <div style="position: absolute; height: 100vh; width: 100vw; background-color: #ccc;" id="js-disabled">
        <style type="text/css">
            /* Make the element disappear if JavaScript isn't allowed */
            .js-only {
                display: none !important;
            }

        </style>

        <div class="row h-100">
            <div class="col-12 col-md-4 offset-md-4 py-5 my-auto">
                <div class="card shadow my-auto">
                    <h4 class="card-header card-title text-center">Javascript is Disabled</h4>

                    <div class="card-body">
                        <p class="card-text">This website required <b>JavaScript</b> to run. Please allow/enable
                            JavaScript and refresh the page.</p>
                        <p class="card-text">If the JavaScript is enabled or allowed, please check your firewall as
                            they might be the one disabling JavaScript.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="js-only">
        <!-- Include Navbar Here -->
        {{-- @include('includes.navbar') --}}
        <!-- Bootstrap NavBar -->
        <nav class="navbar navbar-expand-lg">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                aria-label="Toggle navigation">
                <span><i class="fas fa-bars"></i></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">
                <img class="img-responsive"
                    src="{{ asset('/uploads/settings/' . \App\Settings::where('settings', '=', 'logo')->first()->value) }}"
                    id="logo">
            </a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            My Account
                        </a>

                        <div class="dropdown-menu dropdown-menu-right px-3" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item p-0 mt-2" href="{{ route('home') }}">Home<span
                                    class="float-right"><i class="fas fa-house-user"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('profile') }}">Profile <span
                                    class="float-right"><i class="fas fa-user"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('settings_profile') }}">Settings <span
                                    class="float-right"><i class="fas fa-cog"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('logout') }}">Logout <span
                                    class="float-right"><i class="fas fa-cog"></i></span></a>
                        </div>
                    </li>
                    <!-- This menu is hidden in bigger devices with d-sm-none. 
           The sidebar isn't proper for smaller screens imo, so this dropdown menu can keep all the useful sidebar itens exclusively for smaller screens  -->
                    <li class="nav-item dropdown d-sm-block d-md-none">
                        <a class="nav-link dropdown-toggle" href="#" id="smallerscreenmenu" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false"> Menu </a>
                        <div class="dropdown-menu ropdown-menu-right px-3" aria-labelledby="smallerscreenmenu">
                            <a class="dropdown-item p-0 mt-2" href="{{ route('allbranch') }}">Dashboard<span
                                    class="float-right"><i class="fa fa-dashboard"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('sales.index') }}">Sales Order<span
                                    class="float-right"><i class="fas fa-file-invoice-dollar"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('store.index') }}">Store<span
                                    class="float-right"><i class="fas fa-store"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('user.index') }}">Users<span
                                    class="float-right"><i class="fas fa-users-cog"></i></span></a>
                            <a class="dropdown-item p-0 mt-2"
                                href="{{ route('appointment.index') }}">Appointment<span class="float-right"><i
                                        class="far fa-calendar-check"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('canvass.index') }}">Canvass<span
                                    class="float-right"><i class="fas fa-search-dollar"></i></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('reports.index') }}">Reports<span
                                    class="float-right"><i class="fas fa-exclamation-circle"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('settings.index') }}">Settings<span
                                    class="float-right"><i class="fas fa-cog"></i></span></a>
                            <a class="dropdown-item p-0 mt-2" href="{{ route('others.index') }}">Others<span
                                    class="float-right"><i class="fas fa-ellipsis-h"></i></span></a>
                        </div>
                    </li><!-- Smaller devices menu END -->
                </ul>
            </div>
        </nav><!-- NavBar END -->
        <!-- Bootstrap row -->
        <div class="row p-3" id="body-row">
            <!-- Sidebar -->
            <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
                <!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
                <!-- Bootstrap List Group -->
                <ul class="list-group">
                    <!-- Menu with submenu -->
                    @if (Request::is('admin/dashboard/*'))
                        <a href=" {{ route('allbranch') }} "
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div cla ss="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fa fa-dashboard fa-fw fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Dashboard</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href=" {{ route('allbranch') }} " class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div cla ss="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fa fa-dashboard fa-fw fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Dashboard</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/sales-order/*'))
                        <a href="{{ route('sales.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-file-invoice-dollar fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Sales Order</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('sales.index') }}" class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-file-invoice-dollar fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Sales Order</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/store/*'))
                        <a href="{{ route('store.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-store fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Store</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('store.index') }}" class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-store fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Store</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/user/*'))
                        <a href="{{ route('user.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-users-cog fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Users</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('user.index') }}" class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-users-cog fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Users</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/appointment/*'))
                        <a href="{{ route('appointment.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="far fa-calendar-check fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Appointment</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('appointment.index') }}"
                            class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="far fa-calendar-check fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Appointment</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/canvass/*'))
                        <a href="{{ route('canvass.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-search-dollar fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Canvass</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('canvass.index') }}" class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-search-dollar fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Canvass</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/reports/*'))
                        <a href="{{ route('reports.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-exclamation-circle fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Reports</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('reports.index') }}" class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-exclamation-circle fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Reports</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/settings/*'))
                        <a href="{{ route('settings.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-cog fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Settings</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('settings.index') }}" class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-cog fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Settings</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif
                    @if (Request::is('admin/others/*'))
                        <a href="{{ route('others.index') }}"
                            class="active bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-ellipsis-h fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Others</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @else
                        <a href="{{ route('others.index') }}" class="bg-dark w-100 mx-auto my-1 list-group-item">
                            <div class="w-100 my-auto d-flex justify-content-center">
                                <div class="d-flex justify-content-center">
                                    <div class="container-fluid w-100 text-center px-0 d-block">
                                        <div class="fas fa-ellipsis-h fa-2x side-icon"></div>
                                        <div class="menu-collapsed">Others</div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endif

                    <a href="#" data-toggle="sidebar-colapse" class="bg-dark w-100 mx-auto my-1 list-group-item">
                        <div class="w-100 my-auto d-flex justify-content-center">
                            <div class="d-flex justify-content-center">
                                <div class="container-fluid w-100 text-center px-0 d-block">
                                    <div id="collapse-icon" class="fa fa-2x side-icon"></div>
                                    <div class="menu-collapsed">Collapse</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </ul><!-- List Group END-->
            </div><!-- sidebar-container END -->
            <!-- MAIN -->
            <div class="col p-4">
                <main>
                    @yield('content')
                </main>
            </div><!-- Main Col END -->
        </div><!-- body-row END -->
        <!-- Content Here -->

        @yield('scripts')
</body>

<script type="text/javascript">
    // Hide submenus
    $('#body-row .collapse').collapse('hide');

    // Collapse/Expand icon
    $('#collapse-icon').addClass('fa-angle-double-left');

    // Collapse click
    $('[data-toggle=sidebar-colapse]').click(function() {
        SidebarCollapse();
    });

    function SidebarCollapse() {
        $('.menu-collapsed').toggleClass('d-none');
        $('.sidebar-submenu').toggleClass('d-none');
        $('.submenu-icon').toggleClass('d-none');
        $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');

        // Treating d-flex/d-none on separators with title
        var SeparatorTitle = $('.sidebar-separator-title');
        if (SeparatorTitle.hasClass('d-flex')) {
            SeparatorTitle.removeClass('d-flex');
        } else {
            SeparatorTitle.addClass('d-flex');
        }

        // Collapse/Expand icon
        $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
    }

    @if (Session::has('flash_error'))
        Swal.fire({
        {!! Session::has('has_icon') ? 'icon: `error`,' : '' !!}
        title: `{{ Session::get('flash_error') }}`,
        {!! Session::has('message') ? 'html: `' . Session::get('message') . '`,' : '' !!}
        position: {!! Session::has('position') ? '`' . Session::get('position') . '`' : '`top`' !!},
        showConfirmButton: false,
        toast: {!! Session::has('is_toast') ? Session::get('is_toast') : true !!},
        {!! Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? 'timer: ' . Session::get('duration') . ',' : `timer: 10000,`) : '') : `timer: 10000,` !!}
        background: `#dc3545`,
        customClass: {
        title: `text-white`,
        content: `text-white`,
        popup: `px-3`
        },
        });
    @elseif (Session::has('flash_info'))
        Swal.fire({
        {!! Session::has('has_icon') ? 'icon: `info`,' : '' !!}
        title: `{{ Session::get('flash_info') }}`,
        {!! Session::has('message') ? 'html: `' . Session::get('message') . '`,' : '' !!}
        position: {!! Session::has('position') ? '`' . Session::get('position') . '`' : '`top`' !!},
        showConfirmButton: false,
        toast: {!! Session::has('is_toast') ? Session::get('is_toast') : true !!},
        {!! Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? 'timer: ' . Session::get('duration') . ',' : `timer: 10000,`) : '') : `timer: 10000,` !!}
        background: `#17a2b8`,
        customClass: {
        title: `text-white`,
        content: `text-white`,
        popup: `px-3`
        },
        });
    @elseif (Session::has('flash_success'))
        Swal.fire({
        {!! Session::has('has_icon') ? 'icon: `success`,' : '' !!}
        title: `{{ Session::get('flash_success') }}`,
        {!! Session::has('message') ? 'html: `' . Session::get('message') . '`,' : '' !!}
        position: {!! Session::has('position') ? '`' . Session::get('position') . '`' : '`top`' !!},
        showConfirmButton: false,
        toast: {!! Session::has('is_toast') ? Session::get('is_toast') : true !!},
        {!! Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? 'timer: ' . Session::get('duration') . ',' : `timer: 10000,`) : '') : `timer: 10000,` !!}
        background: `#28a745`,
        customClass: {
        title: `text-white`,
        content: `text-white`,
        popup: `px-3`
        },
        });
    @endif

    $(document).ready(function() {
        $(".col-a").click(function() {
            $('.collapse.show').collapse('hide');
        });
    });
</script>

<script>
    var inputBox = document.getElementById("inputBox");

    var invalidChars = [
        "-",
        "+",
        "e",
        "E",
    ];

    inputBox.addEventListener("keydown", function(e) {
        if (invalidChars.includes(e.key)) {
            e.preventDefault();
        }
    });
</script>

</html>
