<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- jQuery 3.6.0 -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- jQuery UI 1.12.1 -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Removes the code that shows up when script is disabled/not allowed/blocked -->
    <script type="text/javascript" id="for-js-disabled-js">
        $('head').append('<style id="for-js-disabled">#js-disabled { display: none; }</style>');
        $(document).ready(function() {
            $('#js-disabled').remove();
            $('#for-js-disabled').remove();
            $('#for-js-disabled-js').remove();
        });
    </script>

    <!-- popper.js 1.16.0 -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>

    <!-- Bootstrap 4.4 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    {{-- <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="{{asset('js/bootstrap.min.js')}}"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script> --}}

    <!-- Slick Carousel 1.9.0 -->
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js"></script>

    <!-- Sweet Alert 2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <!-- Input Mask 5.0.5 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.5/jquery.inputmask.min.js"></script>

    <!-- Bootstrap 4 Select -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <!-- FontAwesome -->
    <script src="https://kit.fontawesome.com/f67ab1f0a2.js" crossorigin="anonymous"></script>

    <!-- Default CSS Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

    <link rel='icon' href='{{asset("/uploads/settings/" . \App\Settings::where("settings", "=", "logo")->first()->value)}}'>

    {{-- <link rel="icon" href="/images/logo.png"> --}}

    @yield('srcs')

    <title>JAZZ | @yield('title')</title>
</head>

<body>
    <div style="position: absolute; height: 100vh; width: 100vw; background-color: #ccc;" id="js-disabled">
        <style type="text/css">
            /* Make the element disappear if JavaScript isn't allowed */
            .js-only {
                display: none !important;
            }

        </style>

        <div class="row h-100">
            <div class="col-12 col-md-4 offset-md-4 py-5 my-auto">
                <div class="card shadow my-auto">
                    <h4 class="card-header card-title text-center">Javascript is Disabled</h4>

                    <div class="card-body">
                        <p class="card-text">This website required <b>JavaScript</b> to run. Please allow/enable
                            JavaScript and refresh the page.</p>
                        <p class="card-text">If the JavaScript is enabled or allowed, please check your firewall as
                            they might be the one disabling JavaScript.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="js-only">
        <!-- Include Navbar Here -->
        @include('includes.navbar')

        <!-- Content Here -->
        <main>
            @yield('content')
        </main>

        @include('includes.footer')

        @yield('scripts')
</body>

<script type="text/javascript">
    @if (Session::has('flash_error'))
    Swal.fire({
        {!!Session::has('has_icon') ? "icon: `error`," : ""!!}
        title: `{{Session::get('flash_error')}}`,
        {!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
        position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
        showConfirmButton: false,
        toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
        {!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
        background: `#dc3545`,
        customClass: {
            title: `text-white`,
            content: `text-white`,
            popup: `px-3`
        },
    });
    @elseif (Session::has('flash_info'))
    Swal.fire({
        {!!Session::has('has_icon') ? "icon: `info`," : ""!!}
        title: `{{Session::get('flash_info')}}`,
        {!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
        position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
        showConfirmButton: false,
        toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
        {!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
        background: `#17a2b8`,
        customClass: {
            title: `text-white`,
            content: `text-white`,
            popup: `px-3`
        },
    });
    @elseif (Session::has('flash_success'))
    Swal.fire({
        {!!Session::has('has_icon') ? "icon: `success`," : ""!!}
        title: `{{Session::get('flash_success')}}`,
        {!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
        position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
        showConfirmButton: false,
        toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
        {!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
        background: `#28a745`,
        customClass: {
            title: `text-white`,
            content: `text-white`,
            popup: `px-3`
        },
    });
    @endif

    $(document).ready(function(){
        $(".col-a").click(function(){
            $('.collapse.show').collapse('hide');
        });
    });
</script>

</html>
