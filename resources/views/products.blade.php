@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/product.css">
@endsection

@section('title', 'All Products')

@section('content')
    <h1 class="page-title">PRODUCTS</h1>

    <ul class="list-inline d-flex px-5 justify-content-center subnav">
        <li>
            <a class="cursor-pointer" id="menuAll" data-target=".collapse" role="button" aria-expanded="false"
                aria-controls="collapseCar collapseMotor collapseOther">
                <h6 class="px-3 my-auto">ALL</h6>
            </a>
        </li>

        <li>
            <a class="cursor-pointer" data-toggle="collapse" data-target="#collapseCar" aria-expanded="false"
                aria-controls="collapseCar">
                <h6 class="px-3 my-auto">CAR</h6>
            </a>
        </li>

        <li>
            <a class="cursor-pointer" data-toggle="collapse" data-target=".collapseMotor" aria-expanded="false"
                aria-controls="collapseMotor">
                <h6 class="px-3 my-auto">MOTORCYCLE</h6>
            </a>
        </li>

        <li>
            <a class="cursor-pointer" data-toggle="collapse" data-target=".collapseOther" aria-expanded="false"
                aria-controls="collapseOther">
                <h6 class="px-3 my-auto">OTHER</h6>
            </a>
        </li>
    </ul>

    <div class="row w-75 mx-auto my-3 item-collapse">
        <div class="col-lg-12 ">
            <div class="collapse collapseCar" id="collapseCar">
                <div class="container-fluid mx-auto p-3 row">
                    @forelse ($car as $c)
                        <div class="col-lg-4 menu text-left px-3">
                            <a href="#">
                                <h6 class="menu-title">{{ $c->name }}</h6>
                            </a>
                            @foreach ($component as $co)
                                @if ($co->product_type_id == $c->id)
                                    <a href="#">
                                        <h6>{{ $co->name }}</h6>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="collapse collapseMotor" id="collapseMotor">
                <div class="container-fluid mx-auto p-3 row">
                    @forelse ($motor as $m)
                        <div class="col-lg-4 menu text-left px-3">
                            <a href="#">
                                <h6 class="menu-title">{{ $m->name }}</h6>
                            </a>
                            @foreach ($component as $co)
                                @if ($co->product_type_id == $m->id)
                                    <a href="#">
                                        <h6>{{ $co->name }}</h6>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="collapse collapseOther" id="collapseOther">
                <div class="container-fluid mx-auto p-3 row">
                    @forelse ($others as $o)
                        <div class="col-lg-4 menu text-left px-3">
                            <a href="#">
                                <h6 class="menu-title">{{ $o->name }}</h6>
                            </a>
                            @foreach ($component as $co)
                                @if ($co->product_type_id == $o->id)
                                    <a href="#">
                                        <h6>{{ $co->name }}</h6>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid justify-content-end d-flex">
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn my-2 py-0 my-sm-0" type="submit">Search</button>
        </form>
    </div>
    <div class="row m-0 mb-5">
        <div class="col-md-2">
            <div class="container-fluid brands py-3">
                <label>BRANDS</label>
                @forelse ($pBrand as $pb)
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="check{{ $pb->id }}">
                        <label class="form-check-label" for="check{{ $pb->id }}">
                            {{ $pb->name }}
                        </label>
                    </div>
                @empty

                @endforelse
            </div>
        </div>

        <div class="col-md-9">
            <div class="row">

                @foreach ($product as $p)
                    <div class="col-lg-4 col-sm-12 p-3">
                        <div class="card card-course shadow indivcard p-2">
                            <div class="card-body mx-auto">
                                <img width="250" height="250" src="uploads/products/{{ $p->getImg($p->id) }}"
                                    class="card-img-top productImg" alt="productImg">
                                <p class="pt-3" id="prodname">{{ $p->name }}</p>
                                <p id="brand">Brand: {{ $p->pbrand($p->product_brand_id)->name }}</p>
                                <p id="price">Price: &#8369;{{number_format($p->price,2)}}<span class="float-right"><i
                                            class="far fa-heart"></i></span></p>
                            </div>
                            <div class="px-4 pb-4">
                                <a href="{{ route('details', [$p->id]) }}">
                                    <button class="btn w-100 py-2 mb-2">
                                        VIEW ITEM
                                    </button>
                                </a>
                                <button class="btn w-100 py-2">
                                    ADD TO CART
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#menuAll').on('click', (e) => {
                let isShown = true;

                $('.collapse').each((k, v) => {
                    if (!$(v).hasClass('show')) {
                        isShown = false;
                        return;
                    }
                });

                if (isShown)
                    $('.collapse').collapse('hide');
                else
                    $('.collapse').collapse('show');
            });
        });
    </script>
@endsection
