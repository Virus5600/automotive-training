@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/contact.css">
@endsection

@section('title', 'Contact Us')

@section('content')
    <h1 class="page-title">Contact Us</h1>
        <form method="POST" class="form w-75 mx-auto d-block" action="{{ route('storeConcern') }}"
            enctype="multipart/form-data" class="w-100">
            {{ csrf_field() }}
        <div class="form-group mt-5 ">
            @if (!Auth::user())
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="form-group">
                            <label class="text-inverse" for="first_name">FIRST NAME</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" required>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="form-group">
                            <label class="text-inverse" for="last_name">LAST NAME</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" required>
                        </div>
                    </div>
                </div>
                <label for="user_email">EMAIL</label>
                <input class="form-control" id="user_email" name="user_email" required type="email">
            @endif

        </div>
        <label for="concern mt-5">CONCERN</label>
        <textarea class="form-control mb-5" id="concern" name="message" required type="text"></textarea>

        <div class="container-fluid py-3 mb-3 justify-content-end d-flex">
            <button type="submit" class="btn w-50 py-2 px-3 contact-button">SUBMIT</button>
        </div>
    </form>
@endsection

@section('scripts')
@endsection
