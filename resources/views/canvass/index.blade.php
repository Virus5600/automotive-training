@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/service.css">
@endsection

@section('title', 'Canvass')

@section('content')
    <div id="section">
        <h1 class="page-title">CANVASS</h1>
        @foreach ($canvass as $c)
            <div class="row px-4 mx-auto my-4 align-items-center d-flex" id="list">
                <div class="col-md-10 col-sm-12">
                    <h3 class="pt-2">{{ $c->subject_title}}</h3>
                    <p class="limit"> {{ $c->concern }}</p> <br>
                    <h6 class="limit"><b>Admin Remarks: </b> {{ $c->admin_remarks }} </h6>
                </div>
                <div class="col-md-2 col-sm-12">
                    <a href="{{ route('canvass_view', [$c->id]) }}"><button class="btn btn-lg px-5 py-1 mb-2 float-right w-100"
                            type="button">VIEW</button></a>
                </div>
            </div>
        @endforeach
        <div class="row px-4 mx-auto my-4 d-flex align-items-center" id="list">
            <a href="{{ route('canvass_create') }}" id="createCanvass" class="w-100 d-flex">
                <div class="container-fluid align-items-center justify-content-center d-flex">
                    <i class="fas fa-plus p-2"></i>
                    <h3 class="p-2">Create Canvass</h3>
                </div>
            </a>
        </div>
    </div>

@endsection

@section('scripts')
@endsection
