@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/services.css">
@endsection

@section('title', 'Canvass-Create')

@section('content')
    <h1 class="page-title">CANVASS</h1>
    <section class="booking-form w-75 mx-auto d-block">
        <div class="form-group mt-5">
        </div>
        <form method="POST" action="{{ route('storeCanvass') }}" enctype="multipart/form-data" class="w-100">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-12">
                        <label class="text-inverse" for="contact-person">CONTACT PERSON</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="contact-person" name="contact_person" required>

                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="form-group">
                            <label for="contact" class="">CONTACT NO.</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+63</span>
                                </div>
                                <input type="text" class="form-control" name="contact_no" data-mask
                                    data-mask-format="999 999 9999" required>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <label for="subject mt-5">SUBJECT TITLE</label>
            <input class="form-control mb-5" id="subject" name="subject_title" required type="text">
            <label for="concern mt-5">CONCERN</label>
            <textarea class="form-control mb-5" id="concern" name="concern" required type="text"></textarea>

            <div class="container-fluid py-3 ">
                <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="submit"
                        class="btn w-50 py-2 px-3">SUBMIT</button></a>
            </div>
        </form>

    </section>

@endsection

@section('scripts')
    <script>
        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });


        $(document).ready(() => {
            $('#date').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false,
                minDate: new Date('{{ Carbon\Carbon::tomorrow()->timezone('Asia/Manila') }}')
            });
        });
    </script>
@endsection
