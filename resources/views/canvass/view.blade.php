@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/services.css">
@endsection

@section('title', 'Canvass-View')

@section('content')
    <h1 class="page-title">CANVASS</h1>
    <section class="booking-form w-75 mx-auto d-block">

        <div class="form-group">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-12">
                    <label class="text-inverse" for="contact-person">CONTACT PERSON</label>
                    <div class="form-group">
                        <input type="text" class="form-control" value="{{$canvass->contact_person}}" id="contact-person" disabled>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-12">
                    <div class="form-group">
                        <label class="text-inverse" for="contact-no">CONTACT NO.</label>
                        <input type="text" class="form-control" id="contact-no" value="{{$canvass->contact_no}}" disabled data-mask
                            data-mask-format="+63 999 999 9999">
                    </div>
                </div>
            </div>

        </div>
        <label for="subject mt-5">SUBJECT TITLE</label>
        <input class="form-control mb-5" id="subject" name="subject" value="{{$canvass->subject_title}}" disabled type="text">
        <label for="concern mt-5">CONCERN</label>
        <textarea class="form-control mb-5" id="concern" name="concern" disabled type="text">{{$canvass->concern}}</textarea>
        <label for="concern mt-5">ADMIN REMARKS</label>
        <textarea class="form-control mb-5" id="remarks" name="remarks" disabled type="text">{{$canvass->admin_remarks}}</textarea>

        <div class="container-fluid py-3 ">
            <a href="{{ route('canvass') }}" class="w-100 my-2 justify-content-end d-flex"><button type="button" class="btn w-50 py-2 px-3">GO BACK</button></a>
        </div>


    </section>

@endsection
