@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/home.css">
@endsection

@section('title', 'Home')

@section('content')
    <section class="banner">
        <header class="masthead">
            <div class="container-fluid h-100">
                <div class="row h-100 d-flex align-items-center">
                    <div class="col-12">
                        <h1 class="bannertitle text-center">Jazz Automotive Parts and Services</h1>
                        <div class="container text-center my-auto bannerlead">
                            {{ App\Settings::where('settings', '=', 'branch1')->first()->value }}/{{ App\Settings::where('settings', '=', 'branch2')->first()->value }}
                        </div>
                        <div class="text-center my-5"><i class="fas fa-chevron-down"></i></div>
                    </div>
                </div>
            </div>
        </header>
    </section>

    <section class="service">
        <h3 class="text-center pt-5 mt-5">WHAT WE DO:</h3>
        <div class="row p-5 w-100 mx-auto do-content">
            <div class="col-md-4 col-sm-12 container">
                <img class="w-100 d-block mx-auto" src="/images/weSell.png">
                <div class="position-absolute centered h2">WE</div>
                <div class="position-absolute centered h1">SELL</div>
            </div>
            <div class="col-md-4 col-sm-12 container">
                <img class="w-100 d-block mx-auto" src="/images/weRepair.png">
                <div class="position-absolute centered h2">WE</div>
                <div class="position-absolute centered h1">REPAIR</div>
            </div>
            <div class="col-md-4 col-sm-12 container">
                <img class="w-100 d-block mx-auto" src="/images/weInstall.png">
                <div class="position-absolute centered h2">WE</div>
                <div class="position-absolute centered h1">INSTALL</div>
            </div>
        </div>
    </section>

    <section class="featured my-5">
        <div class="container-fluid w-100">
            <h5 class="text-center py-4">FEATURED CAR BRANDS:</h5>
            <div class="wrapper d-flex">
                <div class="carousel">
                    @forelse ($car as $c)
                        <div class="carousel-brands d-flex justify-content-center"><img class="img-fluid m-auto"
                                src="{{ asset('/uploads/carbrands/' . $c->image) }}"></div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </section>

    <section class="products p-3 mx-3 mb-5">
        <div class="row row-cols-1 row-cols-xs-2 row-cols-sm-2 row-cols-lg-4 m-1">
            @foreach ($product as $p)
            <div class="col-lg-4 col-sm-12 p-3">
                <div class="card card-course shadow indivcard p-2">
                    <div class="card-body mx-auto">
                        <img  width="300" height="300" src="uploads/products/{{$p->getImg($p->id)}}" class="card-img-top productImg" alt="productImg">
                        <p class="pt-3" id="prodname">{{$p->name}}</p>
                        <p id="brand">Brand: {{ $p->pbrand($p->product_brand_id)->name }}</p>
                        <p id="price">Price: &#8369;{{number_format($p->price,2)}}<span class="float-right"><i class="far fa-heart"></i></span></p>
                    </div>
                    <div class="px-4 pb-4">
                        <a href="{{ route('details', [$p->id]) }}">
                            <button class="btn w-100 py-2 mb-2">
                                VIEW ITEM
                            </button>
                        </a>
                        <button class="btn w-100 py-2">
                            ADD TO CART
                        </button>
                    </div>
                </div>
            </div>
            @endforeach
            

            <div class="col-lg-12 col-sm-12 text-center py-3 mt-2">
                <button class="mx-auto btn col-lg-4 mb-2 w-100" id="viewmore">VIEW MORE</button>
            </div>

        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.carousel').slick({
                slidesToShow: 4,
                dots: true,
                centerMode: true,
            });
        });

        $(document).ready(() => {
            $('.fa-chevron-down').on('click', () => {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(".service").offset().top - 100
                }, 750);
            });

        });
    </script>

@endsection
