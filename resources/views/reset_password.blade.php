@extends('layout.main')

@section('title', 'Reset Password')

@section('content')
    <div class="container-fluid w-75 my-5 vh-75 bg-car res_pass" style="background-image: url('images/car_bg.png');">
        <h1 class="page-title text-left">Reset account password</h1>
        <h5 class="text-center">Enter a new password for jhaaaaasmin@gmail.com</h5>
        <form autocomplete="off" class="form" id="formLogin" name="formLogin" role="form">
            <label class="mt-2">PASSWORD</label>
            <div class="input-group mb-3">
                <input type="password" class="form-control" id="res_password" name="rpwd" required>
                <div class="input-group-append">
                    <span class="input-group-text" id="res_eyeSlash">
                        <a class="reveal" onclick="visibility1()"><i class="fas fa-eye-slash"
                                aria-hidden="true"></i></a>
                    </span>
                    <span class="input-group-text" id="res_eyeShow" style="display: none;">
                        <a class="reveal" onclick="visibility1()"><i class="fas fa-eye"
                                aria-hidden="true"></i></a>
                    </span>
                </div>
            </div>
            <label class="mt-2">CONFIRM PASSWORD</label>
            <div class="input-group mb-3">
                <input type="password" class="form-control" id="conf_password" name="cpwd" required>
                <div class="input-group-append">
                    <span class="input-group-text" id="conf_eyeSlash">
                        <a class="reveal" onclick="visibility2()"><i class="fas fa-eye-slash"
                                aria-hidden="true"></i></a>
                    </span>
                    <span class="input-group-text" id="conf_eyeShow" style="display: none;">
                        <a class="reveal" onclick="visibility2()"><i class="fas fa-eye"
                                aria-hidden="true"></i></a>
                    </span>
                </div>
            </div>
            <div class="container-fluid d-flex justify-content-end">
                <button class="btn btn-lg w-50 my-3" type="submit">RESET PASSWORD</button>
            </div>
            
        </form>
    </div>




@endsection

@section('scripts')
    <script>
        function visibility1() {
            var x = document.getElementById('res_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#res_eyeShow').show();
                $('#res_eyeSlash').hide();
            } else {
                x.type = "password";
                $('#res_eyeShow').hide();
                $('#res_eyeSlash').show();
            }
        }

        function visibility2() {
            var x = document.getElementById('conf_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#conf_eyeShow').show();
                $('#conf_eyeSlash').hide();
            } else {
                x.type = "password";
                $('#conf_eyeShow').hide();
                $('#conf_eyeSlash').show();
            }
        }
    </script>
@endsection
