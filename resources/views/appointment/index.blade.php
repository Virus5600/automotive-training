@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/service.css">
@endsection

@section('title', 'Appointment')

@section('content')
    <div id="section">
        <h1 class="page-title">APPOINTMENT</h1>
        @foreach ($appointment as $a)
            <div class="row px-4 mx-auto my-4 align-items-center d-flex" id="list">
                <div class="col-md-7 col-sm-12">
                    <div class="row">
                        <div class="col-md-9">
                            <h3 class="pt-2">{{ $a->subject_title }}</h3>
                            <p class="limit">{{ $a->concern }}</p><br>
                            <h6 class="p-0 m-0 limit"><b>Address: </b> {{ $a->address }}</h6>
                        </div>
                        <div class="col-md-3">
                            <p><b>Status: </b> {{ $a->status }}</p>
                            <p><b>Price: </b> {{ $a->price }}</p>
                            <p><b>Payment: </b> {{ $a->payment_status }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-12 row m-auto">
                    <div class="col-lg-12">
                        <a href="{{ route('appointment_view', [$a->id]) }}" class="w-75"><button
                                class="btn btn-lg py-1 mb-2 w-100" type="button">VIEW</button></a>
                    </div>
                    <div class="col-lg-12">
                        <a href="#" class="w-75"><button
                                class="btn btn-lg py-1 mb-2 w-100" type="button">APPOINTMENT DONE</button></a>
                    </div>
                    <div class="col-lg-12">
                        <a href="#" class="w-75"><button
                                class="btn btn-lg py-1 mb-2 w-100" type="button">CANCEL APPOINTMENT</button></a>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="row px-4 mx-auto my-4 d-flex align-items-center" id="list">
            <a href="{{ route('appointment_create') }}" id="createBooking" class="w-100 d-flex">
                <div class="container-fluid align-items-center justify-content-center d-flex">
                    <i class="fas fa-plus p-2"></i>
                    <h3 class="p-2">Schedule Appointment</h3>
                </div>
            </a>
        </div>
    </div>
@endsection

