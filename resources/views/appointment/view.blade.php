@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/services.css">
@endsection

@section('title', 'Booking')

@section('content')
    <h1 class="page-title">APPOINTMENT</h1>
    <section class="booking-form w-75 mx-auto d-block">
        <label for="address">ADDRESS</label>
        <input class="form-control mb-5" id="address" name="address" value=" {{ $appointment->address }} " disabled
            type="text">
        <div class="form-group mt-5">
            <div class="row">
                <div class="col-lg-4 col-sm-4 col-12">
                    <label class="text-inverse" for="status">STATUS</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="status" value="{{ $appointment->status }}" disabled>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-12">
                    <div class="form-group">
                        <label class="text-inverse" for="price">PRICE</label>
                        <input type="text" class="form-control" id="price" value=" {{ $appointment->price }} " disabled>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-4 col-12">
                    <div class="form-group">
                        <label class="text-inverse" for="payment_status">PAYMENT STATUS</label>
                        <input type="text" class="form-control" id="payment_status"
                            value=" {{ $appointment->payment_status }} " disabled>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group mt-5">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-12">
                    <label class="text-inverse" for="date">SELECT DATE</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="date"
                            value="{{ Carbon\Carbon::parse($appointment->appointment_date)->format('M d, Y') }}" disabled>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-12">
                    <div class="form-group">
                        <label class="text-inverse" for="branch">SELECT BRANCH</label>
                        <input type="text" class="form-control" id="branch" value=" {{ $appointment->branch }} "
                            disabled>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-12">
                    <label class="text-inverse" for="contact-person">CONTACT PERSON</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="contact-person"
                            value=" {{ $appointment->contact_person }} " disabled>

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-12">
                    <div class="form-group">
                        <label class="text-inverse" for="contact-no">CONTACT NO.</label>
                        <input type="text" class="form-control" id="contact-no"
                            value=" +63{{ $appointment->contact_no }} " disabled data-mask
                            data-mask-format="+63 999 999 9999">
                    </div>
                </div>
            </div>

        </div>
        <label for="subject">SUBJECT TITLE</label>
        <input class="form-control mb-5" id="subject" name="subject" value=" {{ $appointment->subject_title }} " disabled
            type="text">
        <label for="concern">CONCERN</label>
        <textarea class="form-control mb-5" id="concern" name="concern" disabled
            type="text">{{ $appointment->concern }}</textarea>
        <label for="remarks">ADMIN REMARKS</label>
        <textarea class="form-control mb-5" id="remarks" name="remarks" disabled
            type="text">{{ $appointment->admin_remarks }}</textarea>

        <div class="container-fluid pt-3 ">
            <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="button" class="btn w-50 py-2 px-3">PAY
                    DOWNPAYMENT</button></a>
        </div>
        <div class="container-fluid pb-3 ">
            <a href="{{ route('appointment') }}" class="w-100 my-2 justify-content-end d-flex"><button type="button"
                    class="btn w-50 py-2 px-3">GO BACK</button></a>
        </div>


    </section>

@endsection
