@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="{{ asset('css/service.css') }}">
@endsection

@section('title', 'Booking')

@section('content')
    <h1 class="page-title">APPOINTMENT</h1>
    <section class="booking-form w-75 mx-auto d-block">
        <form method="POST" action="{{ route('storeAppointment') }}" enctype="multipart/form-data" class="w-100">
            {{ csrf_field() }}
            <div class="form-group mt-5">

                <div class="row w-100 mx-auto">
                    <div class="col-lg-9 my-auto">
                        @foreach ($address as $a)
                            @if ($a->is_selected == 1)
                                <h6><b>Address: </b>
                                    {{ $a->address . ', ' . $a->barangay . ', ' . $a->city . ', ' . $a->region }}
                                    <input type="text" class="form-control" id="address" name="address"
                                        style="display: none"
                                        value="{{ $a->address . ', ' . $a->barangay . ', ' . $a->city . ', ' . $a->region }}">
                                </h6>
                            @endif
                        @endforeach
                    </div>
                    <div class="col-lg-3">
                        <a href role="button" data-toggle="modal" data-target="#myModal"
                            class="w-100 my-2 justify-content-end d-flex"><button type="button"
                                class="btn w-100 py-2 px-3">CHANGE
                                ADDRESS</button></a>
                    </div>
                </div>
                <hr class="my-4">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-12">
                        <label class="text-inverse" for="date">SELECT DATE</label>
                        <div class="form-group">
                            <input type="date" class="form-control" id="date" name="appointment_date" required>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="form-group">
                            <label class="text-inverse" for="branch">SELECT BRANCH</label>
                            <select type="text" class="form-control" id="branch" name="branch" required>
                                <option>Choose Branch</option>
                                <option value="1">Branch 1</option>
                                <option value="2">Branch 2</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-12">
                        <label class="text-inverse" for="contact-person">CONTACT PERSON</label>
                        <div class="form-group">
                            <input type="text" class="form-control" id="contact-person" name="contact_person" required>

                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="form-group">
                            <label for="contact" class="">CONTACT NO.</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+63</span>
                                </div>
                                <input type="text" class="form-control" name="contact_no" data-mask
                                    data-mask-format="999 999 9999" required>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <label for="subject mt-5">SUBJECT TITLE</label>
            <input class="form-control mb-5" id="subject" name="subject_title" required type="text">
            <label for="concern mt-5">CONCERN</label>
            <textarea class="form-control mb-5" id="concern" name="concern" required type="text"></textarea>

            <div class="container-fluid py-3 ">
                <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="submit"
                        class="btn w-50 py-2 px-3">SUBMIT</button></a>
            </div>
        </form>

        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-dialog-scrollable modal-xl">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <div class="modal-body">
                        <h3 class="menu-title">Choose Address</h3>
                        @foreach ($address as $a)
                            @if ($a->is_selected == 1)
                                <div class="row px-4 mx-auto my-4 align-items-center d-flex active" id="list">
                                    <div class="col-md-12">
                                        <h3 class="pt-2"> {{ $a->type }} </h3>
                                        <p>{{ $a->address . ', ' . $a->barangay . ', ' . $a->city . ', ' . $a->region }}
                                        </p>
                                    </div>
                                </div>
                            @else
                                <div class="row px-4 mx-auto my-4 align-items-center d-flex" id="list">
                                    <div class="col-md-9 col-sm-12">
                                        <h3 class="pt-2"> {{ $a->type }} </h3>
                                        <p>{{ $a->address . ', ' . $a->barangay . ', ' . $a->city . ', ' . $a->region }}
                                        </p>
                                    </div>
                                    <div class="col-md-3 col-sm-12 ">
                                        <a href="{{ route('activeAddress', [$a->id]) }}"><button
                                                class="btn px-3 py-1 mb-2 text-center w-100"
                                                type="button">SELECT</button></a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn px-3" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

    </section>

@endsection

@section('scripts')
    <script>
        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });


        $(document).ready(() => {
            $('#date').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false,
                minDate: new Date('{{ Carbon\Carbon::tomorrow()->timezone('Asia/Manila') }}')
            });
        });
    </script>
@endsection
