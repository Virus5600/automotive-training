<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row w-100">

            <h1 class="col-md-12 text-uppercase ml-3" id="footerTitle">shop now!</h1>
            
            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3 text-left">

                <!-- Content -->

                <ul class="list-unstyled">
                    <li class="pt-3">
                        <a href="#!">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle-thin fa-stack-2x"></i>
                                <i class="fas fa-map-marker-alt fa-stack-1x"></i>
                            </span>
                        </a> {{App\Settings::where("settings", "=", "branch1")->first()->value}}/{{App\Settings::where("settings", "=", "branch2")->first()->value}}
                    </li>
                    <li>
                        <a href="#!">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle-thin fa-stack-2x"></i>
                                <i class="fas fa-phone-alt fa-stack-1x"></i>
                            </span>
                        </a> {{App\Settings::where("settings", "=", "contact_no")->first()->value}}
                    </li>
                    <li>
                        <a href="{{App\Settings::where("settings", "=", "facebook_link")->first()->value}}">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle-thin fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </span>
                        </a> {{App\Settings::where("settings", "=", "facebook_link")->first()->value}}
                    </li>
                </ul>
            </div>

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright p-3">© 2021 Copyright: Jazz Automotive Parts and Services | <a href role="button" data-toggle="modal" data-target="#myModalTerms">Terms &
        Conditions</a> | <a href role="button" data-toggle="modal" data-target="#myModalPrivacy">Privacy Policy</a>

    </div>
    <!-- Copyright -->

</footer>

<div class="modal fade" id="myModalTerms">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <div class="terms modal-body">
                <h3>Terms & Conditions</h3>
                <p>{{App\Settings::where("settings", "=", "tos")->first()->value}}</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn px-3" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="myModalPrivacy">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <div class="terms modal-body">
                <h3>Privacy Policy</h3>
                <p>{{App\Settings::where("settings", "=", "privacy_policy")->first()->value}}</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn px-3" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<!-- Footer -->
