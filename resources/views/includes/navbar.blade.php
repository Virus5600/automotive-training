<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="{{ route('home') }}"><img class="img-responsive"
            src="{{asset("/uploads/settings/" . \App\Settings::where("settings", "=", "logo")->first()->value)}}" id="logo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span><i class="fas fa-bars"></i></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                @if (Request::is('/'))
                    <a href="{{ route('home') }}" class="nav-link active">HOME</a>
                @else
                    <a href="{{ route('home') }}" class="nav-link">HOME</a>
                @endif
            </li>
            <li class="nav-item">
                @if (Request::is('products'))
                    <a href="{{ route('products') }}" class="nav-link active">ALL PRODUCTS</a>
                @else
                    <a href="{{ route('products') }}" class="nav-link">ALL PRODUCTS</a>
                @endif
            </li>
            <li class="nav-item">
                @if (Request::is('about_us'))
                    <a href="{{ route('about_us') }}" class="nav-link active">ABOUT US</a>
                @else
                    <a href="{{ route('about_us') }}" class="nav-link">ABOUT US</a>
                @endif
            </li>
            <li class="nav-item">
                @if (Request::is('payment_methods'))
                    <a href="{{ route('payment_methods') }}" class="nav-link active">PAYMENT METHODS</a>
                @else
                    <a href="{{ route('payment_methods') }}" class="nav-link">PAYMENT METHODS</a>
                @endif
            </li>
            <li class="nav-item">
                @if (Request::is('contact_us'))
                    <a href="{{ route('contact_us') }}" class="nav-link active">CONTACT US</a>
                @else
                    <a href="{{ route('contact_us') }}" class="nav-link">CONTACT US</a>
                @endif
            </li>
            @if (Auth::check())
                <li class="nav-item dropdown">
                    @if (Request::is('canvass/*') || Request::is('appointment/*'))
                        <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            SERVICES
                        </a>
                    @else
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            SERVICES
                        </a>
                    @endif

                    <div class="dropdown-menu px-3" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item p-0 mt-2" href="{{ route('canvass') }}">CANVASS</a>
                        <a class="dropdown-item p-0 mt-2" href="{{ route('appointment') }}">APPOINTMENT</a>
                    </div>
                </li>
            @else
            @endif
        </ul>
        <ul class="navbar-nav">
            @if (Auth::check())
                <li class="nav-item dropdown">
                    @if (Request::is('mycart/*'))
                        <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span><i class="fas fa-shopping-cart"></i></span> MY CART
                        </a>
                    @else
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <span><i class="fas fa-shopping-cart"></i></span> MY CART
                        </a>
                    @endif

                    <div class="dropdown-menu dropdown-menu-right px-3" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item p-0 mt-2" href="{{ route('wishlist') }}">WISHLIST <span class="float-right"><i class="fas fa-heart"></i></span></a>
                        <a class="dropdown-item p-0 mt-2" href="{{ route('mycart') }}">VIEW CART <span
                                class="float-right"><i class="fas fa-cart-plus"></i></span></a>
                        <a class="dropdown-item p-0 mt-2" href="{{ route('myorder') }}">MY ORDER <span
                                class="float-right"><i class="fas fa-location-arrow"></i></i></span></a>
                        <a class="dropdown-item p-0 mt-2" href="{{ route('checkout') }}">CHECKOUT <span
                                class="float-right"><i class="fas fa-credit-card"></i></span></a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        MY ACCOUNT
                    </a>

                    <div class="dropdown-menu dropdown-menu-right px-3" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item p-0 mt-2" href="{{ route('profile') }}">PROFILE <span
                                class="float-right"><i class="fas fa-user"></i></span></a>
                        <a class="dropdown-item p-0 mt-2" href="{{ route('address_checkout') }}">ADDRESS CHECKOUT <span
                                class="float-right"><i class="fas fa-house-user"></i></span></a>
                        <a class="dropdown-item p-0 mt-2" href="{{ route('settings_profile') }}">SETTINGS <span
                                class="float-right"><i class="fas fa-cog"></i></span></a>
                        @if (Auth::user()->type == 'Admin' || Auth::user()->type == 'Owner')
                            <a class="dropdown-item p-0 mt-2" href="{{ route('allbranch') }}">DASHBOARD <span
                                    class="float-right"><i class="fas fa-chart-line"></i></span></a>
                        @endif
                        <a class="dropdown-item p-0 mt-2" href="{{ route('logout') }}">LOGOUT <span
                                class="float-right"><i class="fas fa-cog"></i></span></a>
                    </div>
                </li>
            @else
                <li class="nav-item">
                    @if (Request::is('login'))
                        <a href="{{ route('login') }}" class="nav-link active">LOG-IN</a>
                    @else
                        <a href="{{ route('login') }}" class="nav-link">LOG-IN</a>
                    @endif
                </li>
                <li class="nav-item">
                    @if (Request::is('register'))
                        <a href="{{ route('register') }}" class="nav-link active">REGISTER</a>
                    @else
                        <a href="{{ route('register') }}" class="nav-link">REGISTER</a>
                    @endif
                </li>
            @endif
        </ul>
    </div>
</nav>
