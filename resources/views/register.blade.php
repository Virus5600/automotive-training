@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/login.css">
@endsection

@section('title', 'Register')

@section('content')
    <div class="row mx-auto py-5 px-4 register-content">
        <div class="col-md-4 align-items-center d-flex" id="forLogo">
            <img src="{{asset("/uploads/settings/" . \App\Settings::where("settings", "=", "logo")->first()->value)}}" class="w-75 mx-auto d-flex">
        </div>
        <div class="col-md-8 p-5" id="forForm">
            <h1 class="page-title w-100 text-left">REGISTER</h1>
            <form method="POST" action="{{ route('userside.store') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}
                <div class="form-group mt-5">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="first_name">FIRST NAME</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="last_name">LAST NAME</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" required>
                            </div>
                        </div>
                    </div>
                    <label for="email">EMAIL</label>
                    <input class="form-control" id="email" name="email" required type="email">
                </div>
                <label class="mt-2">PASSWORD</label>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" id="reg_password" name="password" required>
                    <div class="input-group-append">
                        <span class="input-group-text" id="reg_eyeSlash">
                            <a class="reveal" onclick="visibility1()"><i class="fas fa-eye-slash"
                                    aria-hidden="true"></i></a>
                        </span>
                        <span class="input-group-text" id="reg_eyeShow" style="display: none;">
                            <a class="reveal" onclick="visibility1()"><i class="fas fa-eye"
                                    aria-hidden="true"></i></a>
                        </span>
                    </div>
                </div>
                <label class="mt-2">CONFIRM PASSWORD</label>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" id="conf_password" name="conf_password" required>
                    <div class="input-group-append">
                        <span class="input-group-text" id="conf_eyeSlash">
                            <a class="reveal" onclick="visibility2()"><i class="fas fa-eye-slash"
                                    aria-hidden="true"></i></a>
                        </span>
                        <span class="input-group-text" id="conf_eyeShow" style="display: none;">
                            <a class="reveal" onclick="visibility2()"><i class="fas fa-eye"
                                    aria-hidden="true"></i></a>
                        </span>
                    </div>
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="terms" required>
                        I read and agree on <a href role="button" data-toggle="modal" data-target="#myModalTerms">Terms &
                            Conditions</a>
                    </label>
                </div>
                <div class="container-fluid w-100 justify-content-end d-flex text-center">
                    <button class="btn btn-lg px-5 w-50" id="register" data-action="submit" type="submit">CREATE ACCOUNT</button>
                </div>
            </form>
            <div class="container-fluid w-100 d-flex pt-2">
                <a href="{{ route('login') }}" class="mx-auto" id="textlink">Already have an account? <s
                        class="extra">Sign
                        In.</s></a>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function visibility1() {
            var x = document.getElementById('reg_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#reg_eyeShow').show();
                $('#reg_eyeSlash').hide();
            } else {
                x.type = "password";
                $('#reg_eyeShow').hide();
                $('#reg_eyeSlash').show();
            }
        }

        function visibility2() {
            var x = document.getElementById('conf_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#conf_eyeShow').show();
                $('#conf_eyeSlash').hide();
            } else {
                x.type = "password";
                $('#conf_eyeShow').hide();
                $('#conf_eyeSlash').show();
            }
        }
    </script>
@endsection
