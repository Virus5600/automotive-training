@extends('layout.main')

@section('srcs')
<style>
    #price .fa-heart {
    color: #E5A253;
}

.indivcard {
    border-radius: 45px;
}

</style>
@endsection

@section('title', 'Wishlist')

@section('content')
    <h1 class="page-title">WISHLIST</h1>
    <div class="row row-cols-1 row-cols-xs-2 row-cols-sm-2 row-cols-lg-4 m-1 w-75 mx-auto">


        <div class="col-lg-4 col-sm-12 p-3">
            <div class="card card-course shadow indivcard p-2">
                <div class="card-body">
                    <img src="Images/sample_product.jpg" class="card-img-top" alt="bike">
                    <p class="pt-3" id="prodname">Product Name</p>
                    <p id="brand">Brand</p>
                    <p id="price">Price<span class="float-right"><i class="fas fa-heart"></i></span></p>
                </div>
                <div class="px-4 pb-4">
                    <a href="{{ route('details') }}">
                        <button class="btn w-100 py-2 mb-2">
                            VIEW ITEM
                        </button>
                    </a>
                    <button class="btn w-100 py-2">
                        ADD TO CART
                    </button>
                </div>
            </div>
        </div>
 
    </div>
@endsection

@section('scripts')
@endsection
