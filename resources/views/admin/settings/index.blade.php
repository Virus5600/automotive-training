@extends('layout.admin')

@section('title', 'Settings')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <h1><b>Settings</b></h1>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('settings.update') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-flex flex-column">
                                <img src="{{ $logo->value == null ? '/images/userplaceholder.png' : asset('/uploads/settings/' . $logo->value) }}"
                                    class="mx-auto brand d-flex w-75" id="thumbnail" alt="Profile Image"
                                    onclick="$('#image').trigger('click');" style="cursor: pointer">
                                <input type="file" id="image" name="logo" onchange="swapImg(this);" style="display: none;"
                                    accept=".jpg,.jpeg,.png,.gif,.svg">
                                <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                        SVG</small></small>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="contact_no">CONTACT NO</label>
                                <input type="text" class="form-control" name="contact_no" required value="{{$contact_no->value}}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="facebook_link">FACEBOOK LINK</label>
                                <input type="text" class="form-control" name="facebook_link" required value="{{$facebook_link->value}}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="branch1">BRANCH 1 ADDRESS</label>
                                <textarea type="text" class="form-control" name="branch1" required>{{$branch1->value}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="branch2">BRANCH 2 ADDRESS</label>
                                <textarea type="text" class="form-control" name="branch2" required>{{$branch2->value}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="privacy_policy">PRIVACY POLICY</label>
                                <textarea type="text" class="form-control" name="privacy_policy" required>{{$privacy_policy->value}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="tos">TERMS & CONDITION</label>
                                <textarea type="text" class="form-control" name="tos" required>{{$tos->value}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="about">ABOUT US 1ST PARAGRAPH</label>
                                <textarea type="text" class="form-control" name="about" required>{{$about->value}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="about2">ABOUT US 2ND PARAGRAPH</label>
                                <textarea type="text" class="form-control" name="about2" required>{{$about2->value}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SAVE CHANGES</button>
                    </div>

                </form>
            </div>

        </div>
    </div>

@endsection
