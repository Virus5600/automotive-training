@extends('layout.admin')

@section('title', 'Sales Order')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <h1><b>Sales Order</b></h1>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark">
                <h1 class="m-0 px-3 py-2">Sales Order</h1>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Customer Email</th>
                        <th>Payment Status</th>
                        <th>Status</th>
                        <th>Total Price</th>
                        <th>Delivered Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="7">Nothing to show.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
