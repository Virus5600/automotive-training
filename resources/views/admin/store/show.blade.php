@extends('layout.admin')

@section('title', 'View Product Detail')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('store.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Store Management</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('store.store') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-lg-4 col-sm-12">
                            <div class="form-group">
                                <img src="{{ $img1 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img1) }}" id="thumbnail"
                                    class="img-fluid border w-100 mx-auto" onclick="$('#image').trigger('click');"
                                    style="cursor: pointer">
                                <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                        SVG</small></small>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <div class="form-group">
                                <div class="d-flex flex-column">
                                    <img src="{{ $img2 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img2) }}" id="thumbnail1"
                                        class="img-fluid border w-100 mx-auto" onclick="$('#image1').trigger('click');"
                                        style="cursor: pointer">
                                    <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG,
                                            GIF,
                                            SVG</small></small>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <div class="form-group">
                                <div class="d-flex flex-column">
                                    <img src="{{ $img3 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img3) }}" id="thumbnail2"
                                        class="img-fluid border w-100 mx-auto" onclick="$('#image2').trigger('click');"
                                        style="cursor: pointer">
                                    <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG,
                                            GIF,
                                            SVG</small></small>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">PRODUCT NAME</label>
                                        <input type="text" class="form-control" value="{{ $product->product_name }}"
                                            name="name" disabled>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="inputBox">PRICE</label>
                                        <input type="number" step=".01" min="0" class="form-control" id="inputBox"
                                            name="price" disabled value="{{ $product->price }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">DESCRIPTION</label>
                                <textarea type="text" class="form-control" id="description" name="description"
                                    disabled>{{ $product->description }}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="brand">PRODUCT BRAND</label>
                                        <input class="form-control" name="product_brand_id" disabled
                                            value="{{ $product->pbrand($product->product_brand_id)->name }}">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="category">CATEGORY</label>
                                        <input class="form-control" id="category" name="category" disabled
                                            value="{{ $product->category }}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="type">PRODUCT TYPE</label>
                                        <input class="form-control" id="type" name="product_type_id" disabled
                                            value="{{ $product->ptype($product->product_type_id)->name }}">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="component">PRODUCT COMPONENT</label>
                                        <input class="form-control" id="component" name="product_component_id" disabled
                                            value="{{ $product->pcomponent($product->product_component_id)->name }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
