@extends('layout.admin')

@section('title', 'Add New Product')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('store.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Store Management</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('store.store') }}" enctype="multipart/form-data" class="w-100">
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-lg-4 col-sm-12">
                            <div class="form-group">
                                <img src="/images/placeholder.png" id="thumbnail" class="img-fluid border w-100 mx-auto"
                                    onclick="$('#image').trigger('click');" style="cursor: pointer">
                                <input type="file" id="image" name="img1" onchange="swapImg(this);" style="display: none;"
                                    accept=".jpg,.jpeg,.png,.gif,.svg">
                                <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                        SVG</small></small>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <div class="form-group">
                                <div class="d-flex flex-column">
                                    <img src="/images/placeholder.png" id="thumbnail1"
                                        class="img-fluid border w-100 mx-auto" onclick="$('#image1').trigger('click');"
                                        style="cursor: pointer">
                                    <input type="file" id="image1" name="img2" onchange="swapImg1(this);"
                                        style="display: none;" accept=".jpg,.jpeg,.png,.gif,.svg">
                                    <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG,
                                            GIF,
                                            SVG</small></small>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-12">
                            <div class="form-group">
                                <div class="d-flex flex-column">
                                    <img src="/images/placeholder.png" id="thumbnail2"
                                        class="img-fluid border w-100 mx-auto" onclick="$('#image2').trigger('click');"
                                        style="cursor: pointer">
                                    <input type="file" id="image2" name="img3" onchange="swapImg2(this);"
                                        style="display: none;" accept=".jpg,.jpeg,.png,.gif,.svg">
                                    <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG,
                                            GIF,
                                            SVG</small></small>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">PRODUCT NAME</label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="inputBox">PRICE</label>
                                        <input type="number" step=".01" min="0" class="form-control" id="inputBox"
                                            name="price" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="description">DESCRIPTION</label>
                                <textarea type="text" class="form-control" id="description" name="description"
                                    required></textarea>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="brand">PRODUCT BRAND</label>
                                        <select class="form-control" name="product_brand_id" required>
                                            @foreach ($brand as $b)
                                                <option value="{{ $b->id }}">{{ $b->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="category">CATEGORY</label>
                                        <select class="form-control" id="category" name="category" required>
                                            <option value="Car">Car</option>
                                            <option value="Motor">Motor</option>
                                            <option value="Others">Others</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="type">PRODUCT TYPE</label>
                                        <select class="form-control" id="type" name="product_type_id" required>
                                            @foreach ($type as $t)
                                                <option value="{{ $t->id }}">{{ $t->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="component">PRODUCT COMPONENT</label>
                                        <select class="form-control" id="component" name="product_component_id" required>
                                            @foreach ($component as $c)
                                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="container-fluid d-flex my-3 justify-content-end">
                            <button type="submit" class="btn w-50 px-3 py-2">SUBMIT</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function swapImg(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail").attr("src", "/images/placeholder.png");
            }
        }

        function swapImg1(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail1").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail1").attr("src", "/images/placeholder.png");
            }
        }

        function swapImg2(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail2").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail2").attr("src", "/images/placeholder.png");
            }
        }

        $(document).ready(() => {
            $("#category").on('change', (e) => {
                $.get('{{ route('ajax.getProductType') }}', {
                        category: $(e.target).val()
                    })
                    .done((data) => {
                        let type = $('#type');
                        type.html('');
                        $.each(data.type, (k, v) => {
                            type.append(`<option value="` + v.id + `">` + v.name + `</option>`)
                        });
                    });

                $.get('{{ route('ajax.getProductComponent') }}', {
                        type: $('#type').val()
                    })
                    .done((data) => {
                        let component = $('#component');
                        component.html('');
                        $.each(data.component, (k, v) => {
                            console.log(k);
                            console.log(v);
                            console.log(data);
                            component.append(`<option value="` + v.id + `">` + v.name + `</option>`)
                        });
                    });
            })

            $("#type").on('change', (e) => {
                $.get('{{ route('ajax.getProductComponent') }}', {
                        type: $('#type').val()
                    })
                    .done((data) => {
                        let component = $('#component');
                        component.html('');
                        $.each(data.component, (k, v) => {
                            component.append(`<option value="` + v.id + `">` + v.name + `</option>`)
                        });
                    });
            })
        });
    </script>

@endsection
