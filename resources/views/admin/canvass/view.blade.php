@extends('layout.admin')

@section('title', 'View Canvass')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('canvass.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Canvass</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <section class="booking-form w-75 mx-auto d-block">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-12">
                                <label class="text-inverse" for="contact-person">CONTACT PERSON</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{ $canvass->contact_person }}"
                                        id="contact-person" disabled>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label class="text-inverse" for="contact-no">CONTACT NO.</label>
                                    <input type="text" class="form-control" id="contact-no"
                                        value="{{ $canvass->contact_no }}" disabled data-mask
                                        data-mask-format="+63 999 999 9999">
                                </div>
                            </div>
                        </div>

                    </div>
                    <label for="subject mt-5">SUBJECT TITLE</label>
                    <input class="form-control mb-5" id="subject" name="subject" value="{{ $canvass->subject_title }}"
                        disabled type="text">
                    <label for="concern mt-5">CONCERN</label>
                    <textarea class="form-control mb-5" id="concern" name="concern" disabled
                        type="text">{{ $canvass->concern }}</textarea>
                    <label for="concern mt-5">ADMIN REMARKS</label>
                    <textarea class="form-control mb-5" id="remarks" name="remarks" disabled
                        type="text">{{ $canvass->admin_remarks }}</textarea>
                </section>
            </div>
        </div>
    </div>
@endsection
