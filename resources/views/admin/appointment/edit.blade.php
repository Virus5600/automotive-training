@extends('layout.admin')

@section('title', 'Edit Appointment')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('appointment.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Appointment</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('appointment.update', $appointment->id) }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4 col-12">
                                <label class="text-inverse" for="price">PRICE</label>
                                <div class="form-group">
                                    <input type="number" step=".01" min="0" class="form-control" id="inputBox"
                                        name="price" required value="{{ $appointment->price }}">
                                </div>
                            </div>

                            <div class="col-lg-4 col-sm-4 col-12">
                                <div class="form-group">
                                    <label for="payment_status">PAYMENT STATUS</label>
                                    <select type="text" class="form-control" name="payment_status" required>
                                        @if ($appointment->payment_status == 'Pending')
                                            <option selected value="Pending">Pending</option>
                                            <option value="Paid">Paid</option>
                                        @else
                                            <option value="Pending">Pending</option>
                                            <option selected value="Paid">Paid</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-12">
                                <label class="text-inverse" for="status">STATUS</label>
                                <div class="form-group">
                                    <select type="text" class="form-control" id="status" name="status" required>
                                        @if ($appointment->status == 'Cancelled')
                                            <option selected value="Cancelled">Cancelled</option>
                                        @else
                                            <option value="Cancelled">Cancelled</option>
                                        @endif
                                        @if ($appointment->status == 'Processing')
                                            <option selected value="Processing">Processing</option>
                                        @else
                                            <option value="Processing">Processing</option>
                                        @endif
                                        @if ($appointment->status == 'Confirmed')
                                            <option selected value="Confirmed">Confirmed</option>
                                        @else
                                            <option value="Confirmed">Confirmed</option>
                                        @endif
                                        @if ($appointment->status == 'On The Way')
                                            <option selected value="On The Way">On The Way</option>
                                        @else
                                            <option value="On The Way">On The Way</option>
                                        @endif
                                        @if ($appointment->status == 'Done')
                                            <option selected value="Done">Done</option>
                                        @else
                                            <option value="Done">Done</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <label for="remarks">ADMIN REMARKS</label>
                    <textarea class="form-control mb-5" id="remarks" name="admin_remarks" required type="text">{{$appointment->admin_remarks}}</textarea>

                    <div class="container-fluid py-3 ">
                        <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="submit"
                                class="btn w-50 py-2 px-3">SAVE CHANGES</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
