@extends('layout.admin')

@section('title', 'Appointment')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">

            <div class="row w-100" id="pageTitle">
                <div class="col-lg-9">
                    <h1><b>Appointment Management</b></h1>
                </div>
                <div class="col-lg-3 align-items-center d-flex">
                    <div class="container-fluid">
                        <select class="form-control" onchange="location = this.value;">
                            <option value="{{ route('allbranch') }}">All Branch</option>
                            <option value="{{ route('branch1') }}">Branch 1</option>
                            <option value="{{ route('branch2') }}">Branch 2</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark">
                <h1 class="m-0 px-3 py-2">Appointment</h1>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Appointment Date</th>
                        <th>Branch</th>
                        <th>Email</th>
                        <th>Subject Title</th>
                        <th>Payment Status</th>
                        <th>Status</th>
                        <th>Price</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($appointment as $a)
                        <tr>
                            <td>{{ $a->appointment_date }}</td>
                            <td>{{ $a->branch }}</td>
                            <td>{{ $a->email }}</td>
                            <td>{{ $a->subject_title }}</td>
                            <td>{{ $a->payment_status }}</td>
                            <td>{{ $a->status }}</td>
                            <td>{{ $a->price }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-right action px-3 text-left">
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('appointment.view', [$a->id]) }}"><i
                                                class="fas fa-eye mr-2"></i>View</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('appointment.edit', [$a->id]) }}"><i
                                                class="fas fa-pencil-alt mr-2"></i>Edit</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="8">Nothing to show.</td>
                        </tr>
                    @endforelse

                </tbody>
            </table>
        </div>
    </div>

@endsection
