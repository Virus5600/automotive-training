@extends('layout.admin')

@section('title', 'All Branch')

@section('content')
    <div class="row px-3 dashboard">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-9">
                    <h1><b>Dashboard</b></h1>
                </div>
                <div class="col-lg-3 align-items-center d-flex">
                    <div class="container-fluid">
                        <select class="form-control" onchange="location = this.value;">
                            <option value="{{ route('allbranch') }}">All Branch</option>
                            <option value="{{ route('branch1') }}">Branch 1</option>
                            <option value="{{ route('branch2') }}">Branch 2</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 p-2">
            <div class="container-fluid card-holder p-0 m-0" id="paid">
                <div class="container-fluid p-3 position-absolute ">
                    <h2><b>Paid Total</b></h2>
                    <h2><b>{{$paid_total}}</b></h2>
                </div>

                <div class="d-flex justify-content-end">
                    <img src="/images/paid.png" class="w-100">
                </div>
            </div>
        </div>
        <div class="col-lg-4 p-2">
            <div class="container-fluid card-holder p-0 m-0" id="unpaid">
                <div class="container-fluid p-3 position-absolute ">
                    <h2><b>Unpaid Total</b></h2>
                    <h2><b>{{$unpaid_total}}</b></h2>
                </div>

                <div class="d-flex justify-content-end">
                    <img src="/images/unpaid.png" class="w-100">
                </div>
            </div>
        </div>
        <div class="col-lg-4 p-2">
            <div class="container-fluid card-holder p-0 m-0" id="pending">
                <div class="container-fluid p-3 position-absolute ">
                    <h2><b>Pending Orders</b></h2>
                    <h2><b>{{$pending_order}}</b></h2>
                </div>

                <div class="d-flex justify-content-end">
                    <img src="/images/pending.png" class="w-100">
                </div>
            </div>
        </div>

        <div class="col-lg-6 p-2">
            <div class="container-fluid data-holder p-3 d-flex justify-content-center">
                <div class="row w-100">
                    <div class="col-lg-4 text-center">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="col-lg-8 m-auto">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Total Users</h2>
                            </div>
                            <div class="col-lg-12">
                                <h2>{{ $users }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 p-2">
            <div class="container-fluid data-holder p-3 d-flex justify-content-center">
                <div class="row w-100">
                    <div class="col-lg-4 text-center">
                        <i class="fas fa-truck-pickup"></i>
                    </div>
                    <div class="col-lg-8 m-auto">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Total Services</h2>
                            </div>
                            <div class="col-lg-12">
                                <h2>{{ $total_service }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 p-2">
            <div class="container-fluid data-holder p-3 d-flex justify-content-center">
                <div class="row w-100">
                    <div class="col-lg-4 text-center">
                        <i class="fas fa-box"></i>
                    </div>
                    <div class="col-lg-8 m-auto">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Total Orders</h2>
                            </div>
                            <div class="col-lg-12">
                                <h2>{{ $total_order }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 p-2">
            <div class="container-fluid data-holder p-3 d-flex justify-content-center">
                <div class="row w-100">
                    <div class="col-lg-4 text-center">
                        <i class="fas fa-business-time"></i>
                    </div>
                    <div class="col-lg-8 m-auto">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Pending Services</h2>
                            </div>
                            <div class="col-lg-12">
                                <h2>{{ $pending_service }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark">
                <h3 class="m-0 px-3 py-2">Monthly Earnings</h3>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="2">Nothing to show.</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark">
                <h3 class="m-0 px-3 py-2">Recent Orders</h3>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="2">Nothing to show.</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark">
                <h3 class="m-0 px-3 py-2">Recent Services</h3>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Appointment Date</th>
                        <th>Email</th>
                        <th>Subject Title</th>
                        <th>Payment Status</th>
                        <th>Status</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @forelse ($recent_service as $a)
                        <tr>
                            <td>{{ $a->appointment_date }}</td>
                            <td>{{ $a->email }}</td>
                            <td>{{ $a->subject_title }}</td>
                            <td>{{ $a->payment_status }}</td>
                            <td>{{ $a->status }}</td>
                            <td>{{ $a->price }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="6">Nothing to show.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
