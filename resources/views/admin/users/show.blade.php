@extends('layout.admin')

@section('title', 'Edit User')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <a href="{{ route('user.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>User Management</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form class="w-100">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-flex flex-column">
                                <img src="{{ $users->avatar == null ? '/images/userplaceholder.png' : asset('/uploads/users/' . $users->avatar) }}"
                                    class="mx-auto d-flex w-75 avatar circle-border" id="thumbnail" alt="Profile Image"
                                    onclick="$('#image').trigger('click');" style="cursor: pointer">
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-5">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="first_name">FIRST NAME</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name"
                                        value="{{ $users->first_name }}" disabled>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="last_name">LAST NAME</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name"
                                        value="{{ $users->last_name }}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="email">EMAIL</label>
                                <input type="email" class="form-control" name="email" value="{{ $users->email }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <label for="contact_no">CONTACT NO.</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+63</span>
                                </div>
                                <input type="text" class="form-control" name="contact_no"
                                    value="{{ $users->contact_no }}" disabled>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="type">User Type</label>
                                <input type="text" class="form-control" id="type" name="type"
                                    value="{{ $users->type }}" disabled>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark d-flex">
                <h1 class="m-0 px-3 py-2 mr-5">Address List</h1>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Region</th>
                        <th>City</th>
                        <th>Barangay</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($address as $a)
                        <tr>
                            <td>{{ $a->region }}</td>
                            <td>{{ $a->city }}</td>
                            <td>{{ $a->barangay }}</td>
                            <td>{{ $a->address }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" class="text-center">Nothing to show...</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
