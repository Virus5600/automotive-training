@extends('layout.admin')

@section('title', 'Create New Address')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('user.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>User Management</b></h1>
                    </a>
                    <a href="{{ route('user.edit', [$users->id]) }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>{{ $users->first_name }}</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('user.storeAddress') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}
                    <input name="user_id" value='{{ $users->id }}' style="display: none">

                    <div class="form-group">
                        <label for="type">ADDRESS TYPE</label>
                        <select name="type" class="form-control" id="type" required>
                                <option value="Home">Home</option>
                                <option value="Office">Office</option>
                                <option value="Others">Others</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="region">REGION</label>
                        <select name="region" class="form-control" id="region" required>
                            @foreach (App\ShippingAddress::getRegions() as $r)
                                <option value="{{ $r }}">{{ $r }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="city">CITY</label>
                        <select name="city" class="form-control" id="cities" required>
                            @foreach (App\ShippingAddress::getCities() as $c)
                                <option value="{{ $c }}">{{ $c }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="barangay">BARANGAY</label>
                        <input type="text" name="barangay" id="barangay" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="address">ADDRESS</label>
                        <textarea id="address" name="address" class="form-control" required></textarea>
                    </div>

                    <div class="container-fluid py-3 ">
                        <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="submit"
                                class="btn w-50 py-2 px-3">SUBMIT</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });

        $(document).ready(() => {
            $('#region').on('change', (e) => {
                $.get('{{ route('ajax.getCities') }}', {
                    region: $(e.target).val()
                }).done((data) => {
                    $('#cities').html("");
                    for (let i = 0; i < data.length; i++)
                        $('#cities').append(`<option value="` + data[i] + `">` + data[i] +
                            `</option>`);
                });
            });
        });
    </script>
@endsection
