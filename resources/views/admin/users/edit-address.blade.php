@extends('layout.admin')

@section('title', 'Edit Address')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('user.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>User Management</b></h1>
                    </a>
                    <a href="{{ route('user.edit', [$users->id]) }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>{{ $users->first_name }}</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('user.updateAddress', [$address->id, $users->id]) }}"
                    enctype="multipart/form-data" class="w-100">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="type">ADDRESS TYPE</label>
                        <select name="type" class="form-control" id="type" required>
                            @if ($address->type == 'Home')
                                <option selected value="Home">Home</option>
                            @else
                                <option value="Home">Home</option>
                            @endif
                            @if ($address->type == 'Office')
                                <option selected value="Office">Office</option>
                            @else
                                <option value="Office">Office</option>
                            @endif
                            @if ($address->type == 'Others')
                                <option selected value="Others">Others</option>
                            @else
                                <option value="Others">Others</option>
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="region">REGION</label>
                        <select name="region" class="form-control" id="region" required>
                            @foreach (App\ShippingAddress::getRegions() as $r)
                                @if ($r == $address->region)
                                    <option selected value="{{ $r }}">{{ $r }}</option>
                                @else
                                    <option value="{{ $r }}">{{ $r }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="city">CITY</label>
                        <select name="city" class="form-control" id="cities" required>
                            @foreach (App\ShippingAddress::getCities($address->region) as $c)
                                @if ($c == $address->city)
                                    <option selected value="{{ $c }}">{{ $c }}</option>
                                @else
                                    <option value="{{ $c }}">{{ $c }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="barangay">BARANGAY</label>
                        <input type="text" name="barangay" id="barangay" value="{{ $address->barangay }}"
                            class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="address">ADDRESS</label>
                        <textarea id="address" name="address" class="form-control"
                            required>{{ $address->address }}</textarea>
                    </div>

                    <div class="container-fluid py-3 d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SAVE CHANGES</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });

        $(document).ready(() => {
            $('#region').on('change', (e) => {
                $.get('{{ route('ajax.getCities') }}', {
                    region: $(e.target).val()
                }).done((data) => {
                    $('#cities').html("");
                    for (let i = 0; i < data.length; i++)
                        $('#cities').append(`<option value="` + data[i] + `">` + data[i] +
                            `</option>`);
                });
            });
        });
        
    </script>
@endsection
