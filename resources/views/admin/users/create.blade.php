@extends('layout.admin')

@section('title', 'Create New User')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <a href="{{ route('user.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>User Management</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}

                    <div class="form-group mt-5">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="first_name">FIRST NAME</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="last_name">LAST NAME</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="email">EMAIL</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <label for="contact_no">CONTACT NO.</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+63</span>
                                </div>
                                <input type="text" class="form-control" name="contact_no" data-mask
                                    data-mask-format="999 999 9999" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="type">User Type</label>
                                <select type="text" class="form-control" id="type" name="type" required>
                                    <option value="Admin">Admin</option>
                                    <option value="Owner">Owner</option>
                                    <option value="Client">Client</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SAVE CHANGES</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>

        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });
    </script>
@endsection
