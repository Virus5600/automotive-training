@extends('layout.admin')

@section('title', 'Users')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <h1><b>User Management</b></h1>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark d-flex">
                <h1 class="m-0 px-3 py-2 mr-5">User List</h1>
                <a href="{{ route('user.create') }}" class="my-auto"><button class="btn btn-sm my-2" type="button">
                        Add new user
                    </button></a>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Contact No</th>
                        <th>Type</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $u)
                        <tr>
                            <td>{{ $u->email }}</td>
                            <td>{{ $u->first_name }}</td>
                            <td>{{ $u->last_name }}</td>
                            <td>+63{{ $u->contact_no }}</td>
                            <td>{{ $u->type }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-right action px-3 text-left">
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('user.show', [$u->id]) }}"><i
                                                class="fas fa-eye mr-2"></i>View</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('user.edit', [$u->id]) }}"><i
                                                class="fas fa-pencil-alt mr-2"></i>Edit Details</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('user.change-password', [$u->id]) }}"><i class="fas fa-edit mr-2"></i>Edit
                                            Password</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('user.delete', [$u->id]) }}"><i
                                                class="fas fa-trash mr-2"></i>Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" class="text-center">Nothing to show...</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection
