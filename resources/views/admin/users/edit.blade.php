@extends('layout.admin')

@section('title', 'Edit User')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <a href="{{ route('user.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>User Management</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('user.update', [$users->id]) }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-flex flex-column">
                                <img src="{{ $users->avatar == null ? '/images/userplaceholder.png' : asset('/uploads/users/' . $users->avatar) }}"
                                    class="mx-auto d-flex w-75 avatar circle-border" id="thumbnail" alt="Profile Image"
                                    onclick="$('#image').trigger('click');" style="cursor: pointer">
                                <input type="file" id="image" name="avatar" onchange="swapImg(this);" style="display: none;"
                                    accept=".jpg,.jpeg,.png,.gif,.svg">
                                <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                        SVG</small></small>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-5">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="first_name">FIRST NAME</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" required
                                        value="{{ $users->first_name }}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label for="last_name">LAST NAME</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" required
                                        value="{{ $users->last_name }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="email">EMAIL</label>
                                <input type="email" class="form-control" name="email" value="{{ $users->email }}"
                                    required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <label for="contact_no">CONTACT NO.</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+63</span>
                                </div>
                                <input type="text" class="form-control" name="contact_no" data-mask
                                    data-mask-format="999 999 9999" value="{{ $users->contact_no }}" required>
                            </div>
                        </div>

                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label for="type">User Type</label>
                                <select type="text" class="form-control" id="type" name="type" required>
                                    @if ($users->type == 'Admin')
                                        <option value="Admin">Admin</option>
                                        <option value="Owner">Owner</option>
                                        <option value="Client">Client</option>
                                    @elseif ($users->type == 'Owner')
                                        <option value="Owner">Owner</option>
                                        <option value="Admin">Admin</option>
                                        <option value="Client">Client</option>
                                    @elseif ($users->type == 'Client')
                                        <option value="Client">Client</option>
                                        <option value="Admin">Admin</option>
                                        <option value="Owner">Owner</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SAVE CHANGES</button>
                    </div>

                </form>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark d-flex">
                <h1 class="m-0 px-3 py-2 mr-5">Address List</h1>
                <a href="{{ route('user.createAddress', [$users->id]) }}" class="my-auto"><button class="btn btn-sm my-2" type="button">
                        Add new address
                    </button></a>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Home</th>
                        <th>Region</th>
                        <th>City</th>
                        <th>Barangay</th>
                        <th>Address</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($address as $a)
                        <tr>
                            <td>{{ $a->type }}</td>
                            <td>{{ $a->region }}</td>
                            <td>{{ $a->city }}</td>
                            <td>{{ $a->barangay }}</td>
                            <td>{{ $a->address }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-right action px-3 text-left">
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('user.editAddress', [$a->id, $users->id]) }}"><i
                                                class="fas fa-pencil-alt mr-2"></i>Edit Details</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('user.deleteAddress', [$a->id, $users->id]) }}"><i
                                                class="fas fa-trash mr-2"></i>Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" class="text-center">Nothing to show...</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

    <script>
        function swapImg(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail").attr("src", "/images/userplaceholder.png");
            }
        }

        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });
    </script>
@endsection
