@extends('layout.admin')

@section('title', 'Edit Car Brand')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('others.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Others</b></h1>
                    </a>
                    <a href="{{ route('car.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Car Brands</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('car.update', [$car->id]) }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-flex flex-column">
                                <img src="{{ asset('/uploads/carbrands/' . $car->image) }}" class="mx-auto brand d-flex w-75"
                                    id="thumbnail" alt="Image" onclick="$('#image').trigger('click');"
                                    style="cursor: pointer">
                                <input type="file" id="image" name="image" onchange="swapImg(this);" style="display: none;"
                                    accept=".jpg,.jpeg,.png,.gif,.svg">
                                <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                        SVG</small></small>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">NAME</label>
                                <input type="text" class="form-control" name="name" value="{{ $car->brand }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SAVE CHANGES</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        function swapImg(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail").attr("src", "/images/userplaceholder.png");
            }
        }
    </script>
@endsection
