@extends('layout.admin')

@section('title', 'View Payment Method')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('others.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Others</b></h1>
                    </a>
                    <a href="{{ route('payment.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Payment Method</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form class="w-100">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-flex flex-column">
                                <img src="{{ asset('/uploads/method/' . $payment->logo) }}"
                                    class="mx-auto brand d-flex w-75" id="thumbnail" alt="Image"
                                    onclick="$('#image').trigger('click');" style="cursor: pointer">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">ACCOUNT NAME</label>
                                <input type="text" class="form-control" name="name" value="{{ $payment->name }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="info">ACCOUNT INFORMATION</label>
                                <input type="text" class="form-control" name="info" value="{{ $payment->info }}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="method">PAYMENT METHOD</label>
                                <input type="text" class="form-control" name="method" value="{{ $payment->method }}" disabled>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
