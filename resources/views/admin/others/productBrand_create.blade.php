@extends('layout.admin')

@section('title', 'Add New Product Brand')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('others.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Others</b></h1>
                    </a>
                    <a href="{{ route('pBrand.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Product Brands</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('pBrand.store') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">Product Brand Name</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
