@extends('layout.admin')

@section('title', 'Reports')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <h1><b>Others</b></h1>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark">
                <h1 class="m-0 px-3 py-2">Others</h1>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Payment Method</td>
                        <td>
                            <a class="p-0 mt-2" href="{{ route('payment.index') }}">
                                <button class="btn btn-sm" type="button">
                                    <i class="fas fa-eye mr-2"></i>View
                                </button> </a>
                        </td>
                    </tr>

                    <tr>
                        <td>Product Type</td>
                        <td>
                            <a class="p-0 mt-2" href="{{ route('pType.index') }}">
                                <button class="btn btn-sm" type="button">
                                    <i class="fas fa-eye mr-2"></i>View
                                </button> </a>
                        </td>
                    </tr>

                    <tr>
                        <td>Product Brands</td>
                        <td>
                            <a class="p-0 mt-2" href="{{ route('pBrand.index') }}">
                                <button class="btn btn-sm" type="button">
                                    <i class="fas fa-eye mr-2"></i>View
                                </button> </a>
                        </td>
                    </tr>

                    <tr>
                        <td>Product Component</td>
                        <td>
                            <a class="p-0 mt-2" href="{{route('component.index')}}">
                                <button class="btn btn-sm" type="button">
                                    <i class="fas fa-eye mr-2"></i>View
                                </button> </a>
                        </td>
                    </tr>

                    <tr>
                        <td>Car Brands </td>
                        <td>
                            <a class="p-0 mt-2" href="{{ route('car.index') }}">
                                <button class="btn btn-sm" type="button">
                                    <i class="fas fa-eye mr-2"></i>View
                                </button> </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
