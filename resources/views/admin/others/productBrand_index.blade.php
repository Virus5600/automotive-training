@extends('layout.admin')

@section('title', 'Car Brands')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <a href="{{ route('others.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Others</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark d-flex">
                <h1 class="m-0 px-3 py-2 mr-5">Product Brands</h1>
                <a href="{{ route('pBrand.create') }}" class="my-auto"><button class="btn btn-sm my-2" type="button">
                        Add new brand
                    </button></a>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pbrand as $pb)
                        <tr>
                            <td>{{ $pb->name }}</td>
                            <td>
                                <a class="p-0 mt-2" href="{{ route('pBrand.delete',[$pb->id]) }}">
                                    <button class="btn btn-sm" type="button">
                                        <i class="fas fa-trash mr-2"></i>Delete
                                    </button> </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2" class="text-center">Nothing to show...</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection
