@extends('layout.admin')

@section('title', 'Car Brands')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <a href="{{ route('others.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Others</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark d-flex">
                <h1 class="m-0 px-3 py-2 mr-5">Car Brands</h1>
                <a href="{{ route('car.create') }}" class="my-auto"><button class="btn btn-sm my-2" type="button">
                        Add new brand
                    </button></a>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($cars as $c)
                        <tr>
                            <td>{{ $c->brand }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-right action px-3 text-left">
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('car.show', [$c->id]) }}"><i
                                                class="fas fa-eye mr-2"></i>View</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('car.edit', [$c->id]) }}"><i
                                                class="fas fa-pencil-alt mr-2"></i>Edit</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('car.delete', [$c->id]) }}"><i
                                                class="fas fa-trash mr-2"></i>Delete</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2" class="text-center">Nothing to show...</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection
