@extends('layout.admin')

@section('title', 'Add New Product Type')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('others.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Others</b></h1>
                    </a>
                    <a href="{{ route('pType.index') }}">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Product Types</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <form method="POST" action="{{ route('pType.update', [$type->id]) }}" enctype="multipart/form-data" class="w-100">
                {{ csrf_field() }}
                <div class="container-fluid form w-75">
                    <div class="row">
                        <div class="col-lg-4 my-2">
                            <div class="form-check px-3 my-auto">
                                @if ($type->category == 'Car')
                                    <input class="form-check-input" type="radio" name="category" id="Car" value="Car"
                                        checked>
                                    <label class="form-check-label" for="Car">
                                        <h3>Car</h3>
                                    </label>
                                @else
                                    <input class="form-check-input" type="radio" name="category" id="Car" value="Car">
                                    <label class="form-check-label" for="Car">
                                        <h3>Car</h3>
                                    </label>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4 my-2">
                            <div class="form-check px-3 my-auto">
                                @if ($type->category == 'Motor')
                                    <input class="form-check-input" type="radio" name="category" id="Motor" value="Motor"
                                        checked>
                                    <label class="form-check-label" for="Motor">
                                        <h3>Motor</h3>
                                    </label>
                                @else
                                    <input class="form-check-input" type="radio" name="category" id="Motor" value="Motor">
                                    <label class="form-check-label" for="Motor">
                                        <h3>Motor</h3>
                                    </label>
                                @endif
                            </div>

                        </div>
                        <div class="col-lg-4 my-2">
                            <div class="form-check px-3 my-auto">
                                @if ($type->category == 'Others')
                                    <input class="form-check-input" type="radio" name="category" id="Others" value="Others"
                                        checked>
                                    <label class="form-check-label" for="Others">
                                        <h3> Others </h3>
                                    </label>
                                @else
                                    <input class="form-check-input" type="radio" name="category" id="Others" value="Others">
                                    <label class="form-check-label" for="Others">
                                        <h3> Others </h3>
                                @endif
                            </div>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="name">NAME</label>
                                <input type="text" class="form-control" name="name" value="{{ $type->name }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SUBMIT</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
