@extends('layout.admin')

@section('title', 'Reports')

@section('content')
    <div class="row w-100 table-content">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12">
                    <h1><b>Reports</b></h1>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid w-100 bg-dark">
                <h1 class="m-0 px-3 py-2">Reports</h1>
            </div>
            <table class="table table-striped table-light w-100 m-0 px-3">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Message</th>
                        <th>Admin Remarks</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($report as $r)
                        <tr>
                            <td>{{ $r->first_name }}</td>
                            <td>{{ $r->last_name }}</td>
                            <td>{{ $r->user_email }}</td>
                            <td>{{ $r->message }}</td>
                            <td>{{ $r->admin_remarks }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-right action px-3 text-left">
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('reports.view', [$r->id]) }}"><i
                                                class="fas fa-eye mr-2"></i>View</a>
                                        <a class="dropdown-item p-0 mt-2" href="{{ route('reports.edit', [$r->id]) }}"><i
                                                class="fas fa-pencil-alt mr-2"></i>Edit</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="6">Nothing to show.</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection

