@extends('layout.admin')

@section('title', 'View Report')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('reports.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Reports</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <section class="booking-form w-75 mx-auto d-block">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-12">
                                <label class="text-inverse" for="first_name">FIRST NAME</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{ $report->first_name }}"
                                        id="first_name" disabled>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label class="text-inverse" for="last_name">LAST NAME</label>
                                    <input type="text" class="form-control" id="last_name"
                                        value="{{ $report->last_name }}" disabled>
                                </div>
                            </div>
                        </div>

                    </div>
                    <label for="email">EMAIL</label>
                    <input class="form-control mb-5" id="email" name="email" value="{{ $report->user_email }}"
                        disabled type="text">
                    <label for="message">MESSAGE</label>
                    <textarea class="form-control mb-5" id="message" name="message" disabled
                        type="text">{{ $report->message }}</textarea>
                    <label for="remarks">ADMIN REMARKS</label>
                    <textarea class="form-control mb-5" id="remarks" name="remarks" disabled
                        type="text">{{ $report->admin_remarks }}</textarea>
                </section>
            </div>
        </div>
    </div>
@endsection
