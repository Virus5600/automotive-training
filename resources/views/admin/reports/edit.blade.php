@extends('layout.admin')

@section('title', 'Edit Report')

@section('content')
    <div class="row w-100 table-content mx-auto">
        <div class="col-lg-12">
            <div class="row w-100" id="pageTitle">
                <div class="col-lg-12 d-flex">
                    <a href="{{ route('reports.index') }}" class="mr-2">
                        <h1><i class="fas fa-sm fa-chevron-left mr-2"></i><b>Reports</b></h1>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('reports.update', $report->id) }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}

                    <label for="remarks">ADMIN REMARKS</label>
                    <textarea class="form-control mb-5" id="remarks" name="admin_remarks" required type="text">{{$report->admin_remarks}}</textarea>

                    <div class="container-fluid py-3 ">
                        <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="submit"
                                class="btn w-50 py-2 px-3">SAVE CHANGES</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
