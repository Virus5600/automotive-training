@extends('layout.main')

@section('srcs')
<link rel="stylesheet" href="/css/details.css">
@endsection

@section('title', 'Profile')

@section('content')
<h1 class="page-title">PRODUCT DETAILS</h1>
<div class="row p-5 m-0 image">
	<div class="col-lg-4 col-md-4 px-2 mx-0 my-auto">
		<div class="row m-auto w-100 d-flex justify-content-center">
			<div class="col-lg-12 pb-2 px-0">
				<div id="productImage" style="background-image:url('{{ $img1 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img1) }}')"></div>
			</div>
		</div>

		<div class="row m-auto w-100 d-flex flex-row">
			<div class="col-4 p-0 p-2 d-flex justify-content-center">
				<a href="#" class="thumbnail" style="background-image: url('{{ $img1 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img1) }}')">
					<img src="{{ $img1 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img1) }}">
				</a>
			</div>

			<div class="col-4 p-0 p-2 d-flex justify-content-center">
				<a href="#" class="thumbnail" style="background-image: url('{{ $img2 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img2) }}">
					<img src="{{ $img2 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img2) }}">
				</a>
			</div>

			<div class="col-4 p-0 p-2 d-flex justify-content-center">
				<a href="#" class="thumbnail" style="background-image: url('{{ $img3 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img3) }}')">
					<img src="{{ $img3 == null ? '/images/placeholder.png' : asset('/uploads/products/' . $img3) }}">
				</a>
			</div>
		</div>
	</div>

	<div class="col-md-8 my-5 p-4 border border-dark">
		<div class="container-fluid row">
			<div class="col-lg-9 py-3">
				<h2><b>Product name: </b>{{$product->name}}</h2>
			</div>

			<div class="col-lg-3 py-3 text-center my-auto">
				<i class="far fa-heart"></i>
			</div>

			<div class="col-lg-9 py-3">
				<h4><b>Price: </b>&#8369;{{number_format($product->price,2)}}</h4>
			</div>

			<div class="col-lg-3 py-3 d-flex justify-content-center">
				<div class="def-number-input number-input safari_only">
					<button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"class="minus"></button>
					<input class="quantity" min="0" name="quantity" value="1" type="number">
					<button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"class="plus"></button>
				</div>
			</div>
		</div>

		<div class="container-fluid row my-4 py-3 border mx-auto">
			<div class="col-lg-3 py-3">
				<h4><b>Description: </b></h4>
			</div>

			<div class="col-lg-9 py-3">
				<h4>{{$product->description}}</h4>
			</div>

			<div class="col-lg-3 py-3">
				<h4><b>Brand:</b></h4>
			</div>

			<div class="col-lg-9 py-3">
				<h4>{{ $product->pbrand($product->product_brand_id)->name }}</h4>
			</div>

			<div class="col-lg-3 py-3">
				<h4><b>Category:</b></h4>
			</div>

			<div class="col-lg-9 py-3">
				<h4>{{ $product->category }}</h4>
			</div>
		</div>
	</div>

	<div class="col-lg-12">
		<a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="button" class="btn w-50 py-2 px-3">ADD TO CART</button></a>
	</div>
</div>
@endsection

@section('scripts')
<script>
	$('#productImage').css('background-image', '{{ $img2 == null ? "/images/placeholder.png" : asset("/uploads/products/" . $img2) }}');

	$('a.thumbnail').click(function() {
		var src = $(this).find('img').attr('src');
		var url = "'" + src + "'";
		console.log(url);
		$('#productImage').css('background-image', 'url(' + src + ')');
	});

	$(document).ready(function() {
		var pixelToMove = 50;
		$("#productImage").mousemove(function(e) {
			var width = $(this).innerWidth();
			var height = $(this).innerHeight();
			var newValueX = (e.pageX / width) * pixelToMove;
			var newValueY = (e.pageY / height) * pixelToMove;
			$(this).css('background-position', newValueX + '%' + ' ' + newValueY + '%');
		});
	});
</script>
@endsection