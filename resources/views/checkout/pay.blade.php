@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/checkout.css">
@endsection

@section('title', 'Upload Payment')

@section('content')
    <div class="container-fluid">
        <H1 class="page-title text-left">Upload Payment Receipt</H1>
        <form autocomplete="off" class="form receipt mx-auto bg-car" style="background-image: url('/images/car_bg.png');"
            id="formLogin" name="formLogin" role="form">
            <div class="container-fluid row mx-auto">

                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                            <img src="/images/placeholder.png" id="thumbnail" class="img-fluid border w-100 mx-auto"
                                onclick="$('#image').trigger('click');" style="cursor: pointer">
                            <input type="file" id="image" name="image" onchange="swapImg(this);" style="display: none;"
                                accept=".jpg,.jpeg,.png,.gif,.svg">
                            <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                    SVG</small></small>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <div class="d-flex flex-column">
                            <img src="/images/placeholder.png" id="thumbnail1" class="img-fluid border w-100 mx-auto"
                                onclick="$('#image1').trigger('click');" style="cursor: pointer">
                            <input type="file" id="image1" name="image1" onchange="swapImg1(this);" style="display: none;"
                                accept=".jpg,.jpeg,.png,.gif,.svg">
                            <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                    SVG</small></small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12">
                    <div class="form-group">
                        <div class="d-flex flex-column">
                            <img src="/images/placeholder.png" id="thumbnail2" class="img-fluid border w-100 mx-auto"
                                onclick="$('#image2').trigger('click');" style="cursor: pointer">
                            <input type="file" id="image2" name="image2" onchange="swapImg2(this);" style="display: none;"
                                accept=".jpg,.jpeg,.png,.gif,.svg">
                            <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                    SVG</small></small>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-12 ">
                <a href="#" class="w-100 d-flex justify-content-end"><button id="check_button" class="btn btn-lg w-50 mt-5"
                        type="submit">SUBMIT</button></a>
            </div>
            <div class="col-lg-12 ">
                <a href="{{ route('checkout') }}" class="w-100 d-flex justify-content-end"><button
                    id="check_button" class="btn btn-lg w-50 mt-2 mb-5" type="button">BACK TO CHECKOUT</button></a>
            </div>

        </form>
    </div>
@endsection

@section('scripts')
    <script>
        function swapImg(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail").attr("src", "/images/placeholder.png");
            }
        }

        function swapImg1(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail1").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail1").attr("src", "/images/placeholder.png");
            }
        }

        function swapImg2(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail2").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail2").attr("src", "/images/placeholder.png");
            }
        }
    </script>

@endsection
