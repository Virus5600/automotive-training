@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/checkout.css">
@endsection

@section('title', 'Checkout')

@section('content')
    <div id="section">

        <h1 class="page-title">CHECKOUT</h1>
        <div class="row p-4 mx-auto my-4 align-items-center d-flex" id="list">
            <div class="col-lg-12">
                <h3 class="pt-2">ORDER ID: WEREWRGSDF123</h3>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="row">
                    <div class="col-lg-5 col-sm-12 my-2">
                        <div id="productImage" style="background-image: url('/images/sample_product.jpg')"></div>
                    </div>
                    <div class="col-lg-7 col-sm-12 my-auto">
                        <p><b>Shipping Address:</b>  Blk 8 Lot 13 David st. North Olympus subd. Caloocan City </p>
                        <h3>Status: NOT PAID</h3>
                        <p><b>Please settle the payment for products to be processed</b> </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12 row mx-auto">
                <div class="col-lg-12">
                    <a href="{{ route('checkout_pay') }}" class="w-100"><button class="btn btn-lg py-1 mb-2 w-100" type="button">UPLOAD PAYMENT</button></a>
                </div>
                <div class="col-lg-12">
                    <a href="{{ route('checkout_view') }}" class="w-100"><button class="btn btn-lg py-1 mb-2 w-100" type="button">VIEW ORDER</button></a>
                </div>
                <div class="col-lg-12">
                    <a href="#" class="w-100"><button class="btn btn-lg py-1 mb-2 w-100" type="button">CANCEL ORDER</button></a>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('scripts')
@endsection
