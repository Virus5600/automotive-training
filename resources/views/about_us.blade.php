@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href=" {{ asset('css/about_us.css') }}">
@endsection

@section('title', 'About Us')

@section('content')
    <section class="masthead"></section>
    <h1 class="page-title">ABOUT US</h1>
    <div class="row mb-5 about-content">
        <div class="col-md-4">
            <img src="{{asset("/uploads/settings/" . \App\Settings::where("settings", "=", "logo")->first()->value)}}" class="w-100">
        </div>
        <div class="col-md-8 my-auto">
            <h1 class="text-center" id="shop_name">Jazz Automotive Parts and Services</h1>
            <p class="text-center" id='address'>{{App\Settings::where("settings", "=", "branch1")->first()->value}}/{{App\Settings::where("settings", "=", "branch2")->first()->value}}</p>
        </div>
        <div class="col-md-12 my-4 par">
            {{App\Settings::where("settings", "=", "about")->first()->value}}
        </div>
        <div class="col-md-5 mb-3 par">
            {{App\Settings::where("settings", "=", "about2")->first()->value}}
        </div>
        <div class="col-md-7">
            <img src="/images/image.png" class="w-100">
        </div>
    </div>
@endsection

@section('scripts')
@endsection
