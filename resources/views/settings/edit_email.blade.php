@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/settings.css">
@endsection

@section('title', 'Edit Profile')

@section('content')
    <div class="row">

        <div class="col-md-4 px-4">
            <div class="page-title text-left">SETTINGS</div>
            <div class="container-fluid mx-2 settings">
                @if (Request::is('settings/edit_profile'))
                    <a href="{{ route('settings_profile') }}" class="active">
                        <h3>Edit Profile</h3>
                    </a>
                @else
                    <a href="{{ route('settings_profile') }}">
                        <h3>Edit Profile</h3>
                    </a>
                @endif
                @if (Request::is('settings/change_password'))
                    <a href="{{ route('settings_password') }}" class="active">
                        <h3>Change Password</h3>
                    </a>
                @else
                    <a href="{{ route('settings_password') }}">
                        <h3>Change Password</h3>
                    </a>
                @endif
                @if (Request::is('settings/change_email'))
                    <a href="{{ route('settings_email') }}" class="active">
                        <h3>Change Email</h3>
                    </a>
                @else
                    <a href="{{ route('settings_email') }}">
                        <h3>Change Email</h3>
                    </a>
                @endif
            </div>

        </div>

        <div class="col-md-8 my-5">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form class="w-100">
                    <h3 class="text-center pt-5">CHANGE EMAIL</h3>
                    <h6 class="text-center">NEW EMAIL</h6>
                    <div class="container-fluid mx-auto justify-content-center d-flex">
                        <input type="text" id="barangay" class="form-control w-75">

                    </div>

                    <h6 class="text-center mt-3">
                        If you don't have access to your email, please contact us at
                    </h6>
                    <h6 class="text-center" style="color:#E5A253">jhaaaaasmin@gmail.com</h6>

                    <div class="container-fluid d-flex my-3 justify-content-center">
                        <button type="button" class="btn w-50 px-3 py-2">SAVE CHANGES</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
