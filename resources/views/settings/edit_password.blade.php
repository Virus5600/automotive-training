@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/settings.css">
@endsection

@section('title', 'Edit Password')

@section('content')
    <div class="row">

        <div class="col-md-4 px-4">
            <div class="page-title text-left">SETTINGS</div>
            <div class="container-fluid mx-2 settings">
                @if (Request::is('settings/edit_profile'))
                    <a href="{{ route('settings_profile') }}" class="active">
                        <h3>Edit Profile</h3>
                    </a>
                @else
                    <a href="{{ route('settings_profile') }}">
                        <h3>Edit Profile</h3>
                    </a>
                @endif
                @if (Request::is('settings/change_password'))
                    <a href="{{ route('settings_password') }}" class="active">
                        <h3>Change Password</h3>
                    </a>
                @else
                    <a href="{{ route('settings_password') }}">
                        <h3>Change Password</h3>
                    </a>
                @endif
                @if (Request::is('settings/change_email'))
                    <a href="{{ route('settings_email') }}" class="active">
                        <h3>Change Email</h3>
                    </a>
                @else
                    <a href="{{ route('settings_email') }}">
                        <h3>Change Email</h3>
                    </a>
                @endif
            </div>

        </div>

        <div class="col-md-8 my-5">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('change_password') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}
                    <h3 class="text-center pt-5">CHANGE PASSWORD</h3>
                    <h6 class="py-2">
                        In order to protect your account, make your password:
                    </h6>
                    <h6>
                        - Is longer than 7 characters <br>
                        - Does not match or significantly contain your email <br>
                        - Must include one alphabetic and one numeric character <br>
                    </h6>

                    <label class="mt-2">OLD PASSWORD</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="old_password" name="old_password" required>
                        <div class="input-group-append">
                            <span class="input-group-text" id="old_eyeSlash">
                                <a class="reveal" onclick="visibility()"><i class="fas fa-eye-slash"
                                        aria-hidden="true"></i></a>
                            </span>
                            <span class="input-group-text" id="old_eyeShow" style="display: none;">
                                <a class="reveal" onclick="visibility()"><i class="fas fa-eye"
                                        aria-hidden="true"></i></a>
                            </span>
                        </div>
                    </div>

                    <label class="mt-2">NEW PASSWORD</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="res_password" name="res_password" required>
                        <div class="input-group-append">
                            <span class="input-group-text" id="res_eyeSlash">
                                <a class="reveal" onclick="visibility1()"><i class="fas fa-eye-slash"
                                        aria-hidden="true"></i></a>
                            </span>
                            <span class="input-group-text" id="res_eyeShow" style="display: none;">
                                <a class="reveal" onclick="visibility1()"><i class="fas fa-eye"
                                        aria-hidden="true"></i></a>
                            </span>
                        </div>
                    </div>

                    <label class="mt-2">CONFIRM PASSWORD</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="conf_password" name="res_password_confirmation" required>
                        <div class="input-group-append">
                            <span class="input-group-text" id="conf_eyeSlash">
                                <a class="reveal" onclick="visibility2()"><i class="fas fa-eye-slash"
                                        aria-hidden="true"></i></a>
                            </span>
                            <span class="input-group-text" id="conf_eyeShow" style="display: none;">
                                <a class="reveal" onclick="visibility2()"><i class="fas fa-eye"
                                        aria-hidden="true"></i></a>
                            </span>
                        </div>
                    </div>
                    <div class="container-fluid d-flex justify-content-end">
                        <button class="btn btn-lg w-50 my-3" type="submit">CHANGE PASSWORD</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        function visibility() {
            var x = document.getElementById('old_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#old_eyeShow').show();
                $('#old_eyeSlash').hide();
            } else {
                x.type = "password";
                $('#old_eyeShow').hide();
                $('#old_eyeSlash').show();
            }
        }

        function visibility1() {
            var x = document.getElementById('res_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#res_eyeShow').show();
                $('#res_eyeSlash').hide();
            } else {
                x.type = "password";
                $('#res_eyeShow').hide();
                $('#res_eyeSlash').show();
            }
        }

        function visibility2() {
            var x = document.getElementById('conf_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#conf_eyeShow').show();
                $('#conf_eyeSlash').hide();
            } else {
                x.type = "password";
                $('#conf_eyeShow').hide();
                $('#conf_eyeSlash').show();
            }
        }
    </script>
@endsection
