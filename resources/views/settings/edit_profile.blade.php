@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/settings.css">
@endsection

@section('title', 'Edit Profile')

@section('content')
    <div class="row">

        <div class="col-md-4 px-4">
            <div class="page-title text-left">SETTINGS</div>
            <div class="container-fluid mx-2 settings">
                @if (Request::is('settings/edit_profile'))
                    <a href="{{ route('settings_profile') }}" class="active">
                        <h3>Edit Profile</h3>
                    </a>
                @else
                    <a href="{{ route('settings_profile') }}">
                        <h3>Edit Profile</h3>
                    </a>
                @endif
                @if (Request::is('settings/change_password'))
                    <a href="{{ route('settings_password') }}" class="active">
                        <h3>Change Password</h3>
                    </a>
                @else
                    <a href="{{ route('settings_password') }}">
                        <h3>Change Password</h3>
                    </a>
                @endif
                @if (Request::is('settings/change_email'))
                    <a href="{{ route('settings_email') }}" class="active">
                        <h3>Change Email</h3>
                    </a>
                @else
                    <a href="{{ route('settings_email') }}">
                        <h3>Change Email</h3>
                    </a>
                @endif
            </div>

        </div>

        <div class="col-md-8 my-5">
            <div class="container-fluid form d-flex w-75 justify-content-center">
                <form method="POST" action="{{ route('userside.update') }}" enctype="multipart/form-data"
                    class="w-100">
                    {{ csrf_field() }}
                    <h3 class="text-center pt-5">EDIT PROFILE</h3>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <img src="{{ $users->avatar == null ? '/images/userplaceholder.png' : asset('/uploads/users/' . $users->avatar) }}"
                                class="mx-auto d-flex w-50 edit-avatar circle-border" id="thumbnail" alt="Profile Image"
                                onclick="$('#image').trigger('click');" style="cursor: pointer">
                            <input type="file" id="image" name="avatar" onchange="swapImg(this);" style="display: none;"
                                accept=".jpg,.jpeg,.png,.gif,.svg">
                            <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                                    SVG</small></small>
                        </div>
                    </div>

                    <div class="form-group mt-5">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label class="text-inverse" for="first_name">FIRST NAME</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name"
                                        value="{{ $users->first_name }}" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-12">
                                <div class="form-group">
                                    <label class="text-inverse" for="last_name">LAST NAME</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name"
                                        value="{{ $users->last_name }}" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-3  d-flex">
                            <label for="contact" class="">CONTACT NO.</label>
                        </div>
                        <div class="col-lg-9">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+63</span>
                                </div>
                                <input type="text" class="form-control" name="contact_no" data-mask
                                    data-mask-format="999 999 9999" value="{{ $users->contact_no }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid d-flex my-3 justify-content-end">
                        <button type="submit" class="btn w-50 px-3 py-2">SAVE CHANGES</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function swapImg(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail").attr("src", "/images/userplaceholder.png");
            }
        }

        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });
    </script>
@endsection
