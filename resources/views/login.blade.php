@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/login.css">
@endsection

@section('title', 'Log-in')

@section('content')
    <div class="row mx-auto py-5 px-4 login-content">
        <div class="col-md-4 align-items-center d-flex" id="forLogo">
            <img src="{{asset("/uploads/settings/" . \App\Settings::where("settings", "=", "logo")->first()->value)}}" class="w-75 mx-auto d-flex">
        </div>
        <div class="col-md-8 p-5" id="forForm">
            <h1 class="page-title w-100 text-left">LOG-IN</h1>
            <form autocomplete="off" class="form" id="formLogin" name="formLogin" role="form"
                action="{{ route('authenticate') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group mt-5">
                    <label for="email">EMAIL</label>
                    <input class="form-control" id="email" name="email" required="" type="email">
                </div>
                <label class="mt-2">PASSWORD</label>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" id="login_password" name="pwd" required="">
                    <div class="input-group-append">
                        <span class="input-group-text" id="eyeSlash">
                            <a class="reveal" onclick="visibility3()"><i class="fas fa-eye-slash"
                                    aria-hidden="true"></i></a>
                        </span>
                        <span class="input-group-text" id="eyeShow" style="display: none;">
                            <a class="reveal" onclick="visibility3()"><i class="fas fa-eye"
                                    aria-hidden="true"></i></a>
                        </span>
                    </div>
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="remember"> Remember me
                    </label>
                    <span class="float-right forgot-pass"><a href role="button" data-toggle="modal" data-target="#myModal">Forgot
                            password?</a></span>
                </div>
                <div class="container-fluid w-100 justify-content-end d-flex text-center">
                    <button class="btn btn-lg px-5 w-50" id="login" data-action="submit" type="submit">LOGIN</button>
                </div>
            </form>
            <div class="container-fluid w-100 d-flex pt-2">
                <a href="{{ route('register') }}" class="mx-auto" id="textlink">Not a member? <s
                        class="extra">Sign Up
                        Now.</s></a>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <h3>Forgot your password</h3>
                    <p class="mx-auto">
                        Please enter the email address you'd like your password reset information sent to
                    </p>
                    <label for="email">Email Address</label>
                    <input type="text" id="email" name="email" class="form-control">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn px-3">REQUEST RESET LINK</button>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function visibility3() {
            var x = document.getElementById('login_password');
            if (x.type === 'password') {
                x.type = "text";
                $('#eyeShow').show();
                $('#eyeSlash').hide();
            } else {
                x.type = "password";
                $('#eyeShow').hide();
                $('#eyeSlash').show();
            }
        }
    </script>
@endsection
