@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/profile.css">
@endsection

@section('title', 'Profile')

@section('content')
    <div class="row" id="profile_page">
        <div class="col-md-5 px-4 my-auto align-items-center justify-content-center">
            <div class="d-flex flex-column">
                <img src="{{ $users->avatar == null ? '/images/userplaceholder.png' : asset('/uploads/users/' . $users->avatar) }}"
                    class="mx-auto d-flex w-75 avatar circle-border" id="thumbnail" alt="Profile Image"
                    style="cursor: pointer">
            </div>
            <a href="{{ route('edit_profile') }}"><button type="button" class="btn w-75 mx-auto px-5 py-2 d-block">EDIT
                    PROFILE</button></a>
        </div>
        <div class="col-md-7 my-5">
            <div class="px-0 w-75 mx-4" id="pageTitle">PROFILE</div>
            <div class="row profile p-3 mx-4">
                <div class="col-md-3 mt-3">EMAIL</div>
                        <div class="col-md-8 mt-3">{{ $users->email }}</div>
                        <div class="col-md-3 mt-3">NAME</div>
                        <div class="col-md-8 mt-3">{{ $users->first_name . ' ' . $users->last_name }}</div>
                        <div class="col-md-3 mt-3">CONTACT NO.</div>
                        <div class="col-md-8 mt-3">+63{{ $users->contact_no }}</div>
                        <div class="col-lg-12 mt-3">
                            <hr class="w-75 float-left">
                        </div>
                @forelse ($address as $a)
                     @if ($a->is_selected == 1)  
                        <div class="col-md-3 mt-3">REGION</div>
                        <div class="col-md-8 mt-3">{{ $a->region }}</div>
                        <div class="col-md-3 mt-3">CITY</div>
                        <div class="col-md-8 mt-3">{{ $a->city }}</div>
                        <div class="col-md-3 mt-3">BARANGAY</div>
                        <div class="col-md-8 mt-3">{{ $a->barangay }}</div>
                        <div class="col-md-3 mt-3">ADDRESS</div>
                        <div class="col-md-8 mt-3">{{ $a->address }}</div>
                    @endif
                @empty
                    
                @endforelse
                   
            </div>

        </div>
    </div>
@endsection
