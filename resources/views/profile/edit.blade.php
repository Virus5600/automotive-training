@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/profile.css">
@endsection

@section('title', 'Edit Profile')

@section('content')
    <form method="POST" action="{{ route('userside.update', [$users->id]) }}" enctype="multipart/form-data"
        class="w-100">
        {{ csrf_field() }}
        <div class="row" id="profile_page">
            <div class="col-md-5 px-4 my-auto align-items-center justify-content-center">
                <div class="d-flex flex-column">
                    <img src="{{ $users->avatar == null ? '/images/userplaceholder.png' : asset('/uploads/users/' . $users->avatar) }}"
                        class="mx-auto d-flex w-75 avatar circle-border" id="thumbnail" alt="Profile Image"
                        onclick="$('#image').trigger('click');" style="cursor: pointer">
                    <input type="file" id="image" name="avatar" onchange="swapImg(this);" style="display: none;"
                        accept=".jpg,.jpeg,.png,.gif,.svg">
                    <small class="text-secondary mx-auto"><small><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG, GIF,
                            SVG</small></small>
                </div>
                <button type="submit" class="btn w-75 mx-auto px-5 py-2 d-block">SAVE CHANGES</button>
            </div>
            <div class="col-md-7 my-5">
                <div class="px-0 w-75 mx-2" id="pageTitle">PROFILE</div>
                <div class="form-group w-75 mt-5">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="first_name">FIRST NAME</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $users->first_name }}" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="last_name">LAST NAME</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $users->last_name }}" required>
                            </div>
                        </div>
                    </div>
                    <label for="contact">CONTACT NO.</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">+63</span>
                        </div>
                        <input type="text" class="form-control" name="contact_no" data-mask
                            data-mask-format="999 999 9999" value="{{ $users->contact_no }}" required>
                    </div>
                </div>
                <a href="{{ route('address_checkout') }}"><button type="button" class="btn w-75 mx-auto px-3 my-5 d-block">Edit Shipping Address</button></a>
            </div>
        </div>
    </form>

@endsection

@section('scripts')
    <script src="https://f001.backblazeb2.com/file/buonzz-assets/jquery.ph-locations-v1.0.0.js"></script>
    <script>
        $(document).on('ready load click focus', "[data-mask]", (e) => {
            let obj = $(e.currentTarget);
            if (!obj.attr('data-masked')) {
                obj.inputmask('mask', {
                    'mask': obj.attr('data-mask-format'),
                    'removeMaskOnSubmit': true,
                    'autoUnmask': true
                });

                obj.attr('data-masked', 'true');
            }
        });
        
        function swapImg(obj) {
            if (obj.files && obj.files[0]) {
                let reader = new FileReader();

                reader.onload = function(e) {
                    $("#thumbnail").attr("src", e.target.result);
                }

                reader.readAsDataURL(obj.files[0])
            } else {
                $("#thumbnail").attr("src", "/images/userplaceholder.png");
            }
        }
    </script>
@endsection
