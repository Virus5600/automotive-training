@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/service.css">
@endsection

@section('title', 'Create Address')

@section('content')
    <div class="container-fluid">
        <div class="page-title">CREATE NEW ADDRESS</div>
        <form method="POST" class="form w-75 mx-auto d-block" action="{{ route('storeAddress') }}"
            enctype="multipart/form-data" class="w-100">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="type">ADDRESS TYPE</label>
                <select name="type" class="form-control" id="type" required>
                    <option value="Home">Home</option>
                    <option value="Office">Office</option>
                    <option value="Others">Others</option>
                </select>
            </div>
            <div class="form-group">
                <label for="region">REGION</label>
                <select name="region" class="form-control" id="region" required>
                    @foreach (App\ShippingAddress::getRegions() as $r)
                        <option value="{{ $r }}">{{ $r }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="city">CITY</label>
                <select name="city" class="form-control" id="cities" required>
                    @foreach (App\ShippingAddress::getCities() as $c)
                        <option value="{{ $c }}">{{ $c }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="barangay">BARANGAY</label>
                <input type="text" id="barangay" name="barangay" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="address">ADDRESS</label>
                <textarea id="address" name="address" class="form-control" required></textarea>
            </div>
            <div class="container-fluid py-3 ">
                <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="submit"
                        class="btn w-50 py-2 px-3">SUBMIT</button></a>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(() => {
            $('#region').on('change', (e) => {
                $.get('{{ route('ajax.getCities') }}', {
                    region: $(e.target).val()
                }).done((data) => {
                    $('#cities').html("");
                    for (let i = 0; i < data.length; i++)
                        $('#cities').append(`<option value="` + data[i] + `">` + data[i] +
                            `</option>`);
                });
            });
        });
    </script>
@endsection
