@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/service.css">
@endsection

@section('title', 'Shipping Address')

@section('content')
    <div id="section">
        <h1 class="page-title">SHIPPING ADDRESS</h1>
        @foreach ($address as $a)
            @if ($a->is_selected == 1)
                <div class="row px-4 mx-auto my-4 align-items-center d-flex active" id="list">
                    <div class="col-md-10 col-sm-12">
                        <h3 class="pt-2"> {{ $a->type }} </h3>
                        <p>{{ $a->address . ', ' . $a->barangay . ', ' . $a->city. ', ' . $a->region }}</p>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="{{ route('address_view', [$a->id]) }}"><button
                                class="btn btn-lg px-5 py-1 mb-2 float-right w-100" type="button">VIEW</button></a>
                        <a href="{{ route('address_edit', [$a->id]) }}"><button
                                class="btn btn-lg px-5 py-1 mb-2 float-right w-100" type="button">EDIT</button></a>
                    </div>
                </div>
            @else
                <div class="row px-4 mx-auto my-4 align-items-center d-flex" id="list">
                    <div class="col-md-10 col-sm-12">
                        <h3 class="pt-2"> {{ $a->type }} </h3>
                        <p>{{ $a->address . ', ' . $a->barangay . ', ' . $a->city. ', ' . $a->region }}</p>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <a href="{{ route('address_view', [$a->id]) }}"><button
                                class="btn btn-lg px-5 py-1 mt-2 mb-2 float-right w-100" type="button">VIEW</button></a>
                        <a href="{{ route('activeAddress', [$a->id]) }}"><button class="btn btn-lg px-5 py-1 mb-2 float-right w-100"
                                type="button">SELECT</button></a>
                        <a href="{{ route('address_edit', [$a->id]) }}"><button
                                class="btn btn-lg px-5 py-1 mb-2 float-right w-100" type="button">EDIT</button></a>
                    </div>
                </div>
            @endif
        @endforeach


        <div class="row px-4 mx-auto my-4 d-flex align-items-center" id="list">
            <a href="{{ route('address_create') }}" id="createCanvass" class="w-100 d-flex">
                <div class="container-fluid align-items-center justify-content-center d-flex">
                    <i class="fas fa-plus p-2"></i>
                    <h3 class="p-2">Create New Address</h3>
                </div>
            </a>
        </div>
    </div>

@endsection

@section('scripts')
@endsection
