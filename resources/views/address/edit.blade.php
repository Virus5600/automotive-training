@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/service.css">
@endsection

@section('title', 'Edit Address')

@section('content')
    <div class="container-fluid">
        <div class="page-title">PROFILE</div>
        <form method="POST" class="form w-75 mx-auto d-block" action="{{ route('updateAddress', [$address->id]) }}"
            enctype="multipart/form-data" class="w-100">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="type">ADDRESS TYPE</label>
                <select name="type" class="form-control" id="type" required>
                    @if ($address->type == 'Home')
                        <option selected value="Home">Home</option>
                    @else
                        <option value="Home">Home</option>
                    @endif
                    @if ($address->type == 'Office')
                        <option selected value="Office">Office</option>
                    @else
                        <option value="Office">Office</option>
                    @endif
                    @if ($address->type == 'Others')
                        <option selected value="Others">Others</option>
                    @else
                        <option value="Others">Others</option>
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label for="region">REGION</label>
                <select name="region" class="form-control" id="region" required>
                    @foreach (App\ShippingAddress::getRegions() as $r)
                        @if ($r == $address->region)
                            <option selected value="{{ $r }}">{{ $r }}</option>
                        @else
                            <option value="{{ $r }}">{{ $r }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="city">CITY</label>
                <select name="city" class="form-control" id="city" required>
                    @foreach (App\ShippingAddress::getCities($address->region) as $c)
                        @if ($c == $address->city)
                            <option selected value="{{ $c }}">{{ $c }}</option>
                        @else
                            <option value="{{ $c }}">{{ $c }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="barangay">BARANGAY</label>
                <input type="text" id="barangay" name="barangay" class="form-control" value="{{ $address->barangay }}" required>
            </div>
            <div class="form-group">
                <label for="address">ADDRESS</label>
                <textarea id="address" name="address" class="form-control" required>{{ $address->address }}</textarea>
            </div>
            <div class="container-fluid py-3 ">
                <a href="#" class="w-100 my-2 justify-content-end d-flex"><button type="submit"
                        class="btn w-50 py-2 px-3">SAVE CHANGES</button></a>
                <a href="{{ route('deleteAddress', [$address->id]) }}" class="w-100 justify-content-end d-flex"><button type="button"
                        class="btn w-50 py-2 px-3">DELETE</button></a>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(() => {
            $('#region').on('change', (e) => {
                $.get('{{ route('ajax.getCities') }}', {
                    region: $(e.target).val()
                }).done((data) => {
                    $('#cities').html("");
                    for (let i = 0; i < data.length; i++)
                        $('#cities').append(`<option value="` + data[i] + `">` + data[i] +
                            `</option>`);
                });
            });
        });
    </script>
@endsection
