@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/service.css">
@endsection

@section('title', 'View Address')

@section('content')
<div class="container-fluid">
        <div class="page-title">ADDRESS</div>
        <form autocomplete="off" class="form w-75 mx-auto d-block" id="formLogin" name="formLogin" role="form">
            <div class="form-group">
                <label for="region">REGION</label>
                <input name="region" class="form-control" disabled id="region" value="{{ $address->region }}">
            </div>
            <div class="form-group">
                <label for="city">CITY</label>
                <input name="city" class="form-control"disabled  id="city" value="{{ $address->city }}">
            </div>
            <div class="form-group">
                <label for="barangay">BARANGAY</label>
                <input type="text" id="barangay" disabled class="form-control" value="{{ $address->barangay }}">
            </div>
            <div class="form-group">
                <label for="address">ADDRESS</label>
                <textarea id="address" class="form-control" disabled>{{ $address->address }}</textarea>
            </div>
            <div class="container-fluid py-3 justify-content-end d-flex">
                <a href="{{ route('address_checkout', [$address->user_id]) }}"><button type="button" class="btn w-100 py-2 px-3">GO BACK</button></a>
            </div>
        </form>
</div>
@endsection
