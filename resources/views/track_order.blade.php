@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/checkout.css">
@endsection

@section('title', 'Track Order')

@section('content')
    <div id="section">

        <h1 class="page-title">Track Order</h1>
        <div class="row p-4 mx-auto my-4 align-items-center d-flex" id="list">
            <div class="col-lg-12">
                <h3 class="pt-2">ORDER ID: WEREWRGSDF123</h3>
            </div>
            <div class="col-lg-12">
                <p><b>Shipping Address:</b> Blk 8 Lot 13 David st. North Olympus subd. Caloocan City </p>
            </div>
            <div class="col-lg-3 p-0">
                <p class="text-center"><b>Order Confirmed</b></p>
                <img src="/images/conf_order.png" class="w-100">
            </div>
            <div class="col-lg-3 p-0">
                <p class="text-center"><b>Order Confirmed</b></p>
                <img src="/images/ship.png" class="w-100">
            </div>
            <div class="col-lg-3 p-0">
                <p class="text-center"><b>Order Confirmed</b></p>
                <img src="/images/omw.png" class="w-100">
            </div>
            <div class="col-lg-3 p-0">
                <p class="text-center"><b>Order Confirmed</b></p>
                <img src="/images/pickup.png" class="w-100">
            </div>

        </div>
        <h3 class="text-center">Order List</h3>
        <div class="row p-4 mx-auto my-4" id="list">
            <div class="col-lg-3 row mx-auto">
                <div class="col-lg-12 col-sm-12 my-2">
                    <div id="productImage" style="background-image: url('/images/sample_product.jpg')"></div>
                </div>
                <div class="col-lg-12 col-sm-12 my-auto">
                    <h3>Price: 200PHP</h3>
                    <p><b>Item name</b></p>
                    <p>Quantity: 1</p>
                </div>
            </div>

            <div class="col-lg-3 row mx-auto">
                <div class="col-lg-12 col-sm-12 my-2">
                    <div id="productImage" style="background-image: url('/images/sample_product.jpg')"></div>
                </div>
                <div class="col-lg-12 col-sm-12 my-auto">
                    <h3>Price: 200PHP</h3>
                    <p><b>Item name</b></p>
                    <p>Quantity: 1</p>
                </div>
            </div>

            <div class="col-lg-3 row mx-auto">
                <div class="col-lg-12 col-sm-12 my-2">
                    <div id="productImage" style="background-image: url('/images/sample_product.jpg')"></div>
                </div>
                <div class="col-lg-12 col-sm-12 my-auto">
                    <h3>Price: 200PHP</h3>
                    <p><b>Item name</b></p>
                    <p>Quantity: 1</p>
                </div>
            </div>

            <div class="col-lg-3 row mx-auto">
                <div class="col-lg-12 col-sm-12 my-2">
                    <div id="productImage" style="background-image: url('/images/sample_product.jpg')"></div>
                </div>
                <div class="col-lg-12 col-sm-12 my-auto">
                    <h3>Price: 200PHP</h3>
                    <p><b>Item name</b></p>
                    <p>Quantity: 1</p>
                </div>
            </div>

            <div class="col-lg-12">
                <a href="{{ route('myorder') }}" class="w-100"><button class="btn btn-lg py-1 mb-2 w-100" type="button">BACK TO MY ORDER</button></a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
