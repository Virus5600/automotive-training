@extends('layout.main')

@section('srcs')
    <link rel="stylesheet" href="/css/mycart.css">
@endsection

@section('title', 'My Cart')

@section('content')
    {{-- <div class="container-fluid d-flex justify-content-center">
        <img src="/images/emptyCart.png" class="w-50 p-5">
    </div> --}}

    <h1 class="page-title">MY CART</h1>
    <div class="row w-90 mx-auto">
        <div class="col-lg-9 my-auto">
            @foreach ($address as $a)
                @if ($a->is_selected == 1)
                    <h6><b>Shipping Address: </b>
                        {{ $a->address . ', ' . $a->barangay . ', ' . $a->city . ', ' . $a->region }}
                    </h6>
                @endif
            @endforeach
        </div>
        <div class="col-lg-3">
            <a href role="button" data-toggle="modal" data-target="#myModal"
                class="w-100 my-2 justify-content-end d-flex"><button type="button" class="btn w-100 py-2 px-3">CHANGE
                    ADDRESS</button></a>
        </div>
    </div>

    <div class="row w-90 mx-auto">
        <div class="col-lg-8 my-3">
            <div class="row w-100 mx-auto">
                <div class="col-lg-12 py-4 px-2 mx-auto row border border-radius border-dark">
                    <div class="col-lg-12 text-right my-auto">
                        <i class="fas fa-trash"></i>
                    </div>
                    <div class="col-lg-4">
                        <div class="image border border-dark" style="background-image: url('/images/sample_product1.jpg')">
                        </div>
                    </div>
                    <div class="col-lg-8 my-auto">
                        <div class="row">
                            <div class="col-lg-3">
                                <h3><b>Price:</b></h3>
                            </div>
                            <div class="col-lg-8">
                                <h3>9999php</h3>
                            </div>
                            <div class="col-lg-3 pt-3">
                                <h6><b>Item:</b></h6>
                            </div>
                            <div class="col-lg-8 pt-3">
                                <h6>Petron sprint</h6>
                            </div>
                            <div class="col-lg-12 pt-5">
                                <div class="def-number-input number-input safari_only">
                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                        class="minus"></button>
                                    <input class="quantity" min="0" name="quantity" value="1" type="number">
                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                        class="plus"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 py-4 px-2 mx-auto row border border-radius border-dark">
                    <div class="col-lg-12 text-right my-auto">
                        <i class="fas fa-trash"></i>
                    </div>
                    <div class="col-lg-4">
                        <div class="image border border-dark" style="background-image: url('/images/sample_product1.jpg')">
                        </div>
                    </div>
                    <div class="col-lg-8 my-auto">
                        <div class="row">
                            <div class="col-lg-3">
                                <h3><b>Price:</b></h3>
                            </div>
                            <div class="col-lg-8">
                                <h3>9999php</h3>
                            </div>
                            <div class="col-lg-3 pt-3">
                                <h6><b>Item:</b></h6>
                            </div>
                            <div class="col-lg-8 pt-3">
                                <h6>Petron sprint</h6>
                            </div>
                            <div class="col-lg-12 pt-5">
                                <div class="def-number-input number-input safari_only">
                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                        class="minus"></button>
                                    <input class="quantity" min="0" name="quantity" value="1" type="number">
                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                        class="plus"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 py-4 px-2 mx-auto row border border-radius border-dark">
                    <div class="col-lg-12 text-right my-auto">
                        <i class="fas fa-trash"></i>
                    </div>
                    <div class="col-lg-4">
                        <div class="image border border-dark" style="background-image: url('/images/sample_product1.jpg')">
                        </div>
                    </div>
                    <div class="col-lg-8 my-auto">
                        <div class="row">
                            <div class="col-lg-3">
                                <h3><b>Price:</b></h3>
                            </div>
                            <div class="col-lg-8">
                                <h3>9999php</h3>
                            </div>
                            <div class="col-lg-3 pt-3">
                                <h6><b>Item:</b></h6>
                            </div>
                            <div class="col-lg-8 pt-3">
                                <h6>Petron sprint</h6>
                            </div>
                            <div class="col-lg-12 pt-5">
                                <div class="def-number-input number-input safari_only">
                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()"
                                        class="minus"></button>
                                    <input class="quantity" min="0" name="quantity" value="1" type="number">
                                    <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()"
                                        class="plus"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 mx-auto my-3">
            <div class="col-lg-12 py-4 px-2 border border-radius border-dark">
                <div class="row px-3">
                    <div class="col-lg-4"><b>ORDER ID:</b></div>
                    <div class="col-lg-8">23JGK9730FM</div>
                    <div class="col-lg-12">
                        <h2 class="mx-auto my-4" style="border-bottom:1px solid black">TOTAL</h2>
                    </div>
                    <div class="col-lg-6">
                        <h5><b>Sub-total</b></h5>
                    </div>
                    <div class="col-lg-6">
                        <h5>PHP 999.00</h5>
                    </div>
                    <div class="col-lg-6">
                        <h5><b>Delivery</b></h5>
                    </div>
                    <div class="col-lg-6">
                        <h5>PHP 999.00</h5>
                    </div>
                    <div class="col-lg-12">
                        <select name="" id="" class="form-control my-4">
                            <option value="">Standard Delivery [200php]</option>
                        </select>
                    </div>
                    <div class="col-lg-12">
                        <h2 class="text-right total">PHP9999999.00</h2>
                    </div>
                    <div class="col-lg-12 my-4 justify-content-center d-flex">
                        <button class="btn px-5 py-2">CHECKOUT</button>
                    </div>
                    <div class="col-lg-12">
                        <h6>WE ACCEPT:</h6>
                        <div class="row">
                            @foreach ($payment as $p)
                            <div class="col-lg-3">
                                <img src="uploads/method/{{ $p->logo}}" class="w-100">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-scrollable modal-xl">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <h3 class="menu-title">Choose Address</h3>
                    @foreach ($address as $a)
                        @if ($a->is_selected == 1)
                            <div class="row px-4 mx-auto my-4 align-items-center d-flex active" id="list">
                                <div class="col-md-12">
                                    <h3 class="pt-2"> {{ $a->type }} </h3>
                                    <p>{{ $a->address . ', ' . $a->barangay . ', ' . $a->city . ', ' . $a->region }}</p>
                                </div>
                            </div>
                        @else
                            <div class="row px-4 mx-auto my-4 align-items-center d-flex" id="list">
                                <div class="col-md-9 col-sm-12">
                                    <h3 class="pt-2"> {{ $a->type }} </h3>
                                    <p>{{ $a->address . ', ' . $a->barangay . ', ' . $a->city . ', ' . $a->region }}</p>
                                </div>
                                <div class="col-md-3 col-sm-12 ">
                                    <a href="{{ route('activeAddress', [$a->id]) }}"><button
                                            class="btn px-3 py-1 mb-2 text-center w-100" type="button">SELECT</button></a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn px-3" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
