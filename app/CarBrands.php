<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarBrands extends Model
{
    protected $fillable = [
    	'brand',
		'image',
	];
}
