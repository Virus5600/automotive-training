<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductType;

class ProductComponent extends Model
{
   protected $fillable = [
      'product_type_id',
      'name',
  ];

  // Relationship
  protected function productType() { return $this->belongsTo('App\ProductType'); }

  public function getName($id) {
      return ProductType::find($id);
   }

}
