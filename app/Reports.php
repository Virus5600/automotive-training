<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
    protected $fillable = [
		'user_email',
		'first_name',
		'last_name',
		'message',
        'admin_remarks',
	];

}
