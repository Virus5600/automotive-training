<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProductType;
use App\ProductComponent;

use DB;
use Exception;
use Log;

class AjaxController extends Controller
{
    protected function getProductType(Request $req){
        $type = ProductType::where('category', '=', $req->category)->get();

        return response()->json([
            'type' => $type
        ]);
    }

    protected function getProductComponent(Request $req){
        $component = ProductComponent::where('product_type_id', '=', $req->type)->get();
        Log:info($component);
        return response()->json([
            'component' => $component
        ]);
    }
}
