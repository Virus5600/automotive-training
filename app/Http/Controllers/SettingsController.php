<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Settings;

use DB;
use Exception;
use Validator;

class SettingsController extends Controller
{
    protected function index() {
        return view ('admin.settings.index', [
            'branch1' => Settings::where('settings', '=', 'branch1')->first(),
            'branch2' => Settings::where('settings', '=', 'branch2')->first(),
            'contact_no' => Settings::where('settings', '=', 'contact_no')->first(),
            'facebook_link' => Settings::where('settings', '=', 'facebook_link')->first(),
            'logo' => Settings::where('settings', '=', 'logo')->first(),
            'privacy_policy' => Settings::where('settings', '=', 'privacy_policy')->first(),
            'tos' => Settings::where('settings', '=', 'tos')->first(),
            'about' => Settings::where('settings', '=', 'about')->first(),
            'about2' => Settings::where('settings', '=', 'about2')->first(),
        ]);
    }

    public function update(Request $req) {
		$validator = Validator::make($req->all(), [
			'branch1' => 'required',
			'branch2' => 'required',
			'contact_no' => 'required',
			'facebook_link' => 'required',
			'logo' => 'image|mimes:jpeg,png,jpg,ico',
			'privacy_policy' => 'required',
			'tos' => 'required',
			'about' => 'required',
			'about2' => 'required',

		]);

        // dd($validator);
		
		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			// FILE HANDLING AREA
			$logo = Settings::where('settings', '=', 'logo')->first();
			if ($req->hasFile('logo')) {
				if ($logo->value != null)
					File::delete(public_path() . '/uploads/settings/' . $logo->value);
		
				$req_logo = null;
				$destination = 'uploads/settings';
				$fileType = $req->file('logo')->getClientOriginalExtension();
				$req_logo = "favicon-alt." . $fileType;
				$req->file('logo')->move($destination, $req_logo);
				$logo->value = $req_logo;
				$logo->save();
			}
			
			// OTHER COLUMNS
			$cols = ['branch1', 'branch2', 'contact_no', 'facebook_link', 'privacy_policy', 'tos', 'about', 'about2'];
			foreach ($cols as $v) {
				$settings = Settings::where('settings', '=', $v)->first();
				$settings->value = $req->get($v);
				$settings->save();
			}

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully Updated Website\'s Details');
	}
}
