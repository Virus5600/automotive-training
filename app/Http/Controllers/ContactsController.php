<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Reports;

use Auth;
use DB;
use Exception;
use Validator;


class ContactsController extends Controller
{

    //CONTACT US

	protected function storeConcern(Request $req) {

        if (!Auth::user()) {
            $validator = Validator::make($req->all(), [
                'first_name' => 'required|min:2',
                'last_name' => 'required|min:2',
                'user_email' => 'required',
                'message' => 'required',
            ]);
        } else {
            $validator = Validator::make($req->all(), [
                'message' => 'required',
            ]);
        }

		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
		
		try {
			DB::beginTransaction();

            if (!Auth::user()) {
                $r = Reports::create([
                    'first_name' => $req->first_name,
                    'last_name' => $req->last_name,
                    'user_email' => $req->user_email,
                    'message' => $req->message,
                ]);
            }
            
            if(Auth::user()){
                $r = Reports::create([
                    'first_name' => Auth::user()->first_name,
                    'last_name' => Auth::user()->last_name,
                    'user_email' => Auth::user()->email,
                    'message' => $req->message,
                ]);
            }

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
            dd($e);
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Submitted message.');
	}


    // ADMIN SIDE

    protected function index() {
		$report = Reports::orderBy('ID', 'DESC')->get();

		return view ('admin.reports.index', [
			'report' => $report
		]);
	}

	protected function view($id) {
		$report = Reports::find($id);

		return view ('admin.reports.view', [
			'report' => $report
		]);
	}

	protected function edit($id) {
		$report = Reports::find($id);

		return view ('admin.reports.edit', [
			'report' => $report
		]);
	}

	protected function update(Request $req, $id) {
		$report = Reports::find($id);

		$validator = Validator::make($req->all(), [
			'admin_remarks' => 'required|min:2',
			]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		try {
			DB::beginTransaction();

			$report->admin_remarks = $req->admin_remarks;

			$report->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('reports.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('reports.index')
			->with('flash_success', 'Successfully updated report.');
	}

}
