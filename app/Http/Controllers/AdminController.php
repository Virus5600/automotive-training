<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\SalesOrder;
use App\Services;

class AdminController extends Controller
{
    protected function allbranch() {
		$paid_order = SalesOrder::where('payment_status', '=', 'Paid')->count();

		$unpaid_order = SalesOrder::where('payment_status', '=', 'Pending')->count();

		$pending_order = SalesOrder::where('status', '<>', 'Cancelled')
			->where('status', '<>', 'Delivered')
			->count();

		$paid_service = Services::where('payment_status', '=', 'Paid')->count();

		$unpaid_service = Services::where('type', '=', 'Appointment')
			->where('payment_status', '=', 'Pending')->count();

		$pending_service = Services::where('type', '=', 'Appointment')
			->where('status', '<>', 'Cancelled')
			->where('status', '<>', 'Delivered')
			->count();

		$total_order = SalesOrder::where('status', '<>', 'Cancelled')->count();

		$total_service = Services::where('status', '<>', 'Cancelled')->count();

		$paid_total = $paid_order + $paid_service;

		$unpaid_total = $pending_order + $unpaid_service;		

		$recent_service = Services::where('type', '=', 'Appointment')
			->orderBy('ID', 'DESC')->take(5)->get();

		return view ('admin.dashboard.allbranch',[
			'paid_order' => $paid_order,
			'pending_order' => $pending_order,
			'paid_service' => $paid_service,
			'unpaid_service' => $unpaid_service,
			'pending_service' => $pending_service,
			'total_order' => $total_order,
			'total_service' => $total_service,
			'paid_total' => $paid_total,
			'unpaid_total' => $unpaid_total,
			'users' => User::count(),
			'recent_service' => $recent_service
		]);
	}

	protected function branch1() {
		$paid_order = SalesOrder::where('payment_status', '=', 'Paid')->count();

		$unpaid_order = SalesOrder::where('payment_status', '=', 'Pending')->count();

		$pending_order = SalesOrder::where('status', '<>', 'Cancelled')
			->where('status', '<>', 'Delivered')
			->count();

		$total_order = SalesOrder::where('status', '<>', 'Cancelled')->count();

		$recent_service = Services::where('type', '=', 'Appointment')
			->where('branch', '=', 'branch1')
			->orderBy('ID', 'DESC')->take(5)->get();

		return view ('admin.dashboard.branch1', [
			'paid_order' => $paid_order,
			'unpaid_order' => $unpaid_order,
			'pending_order' => $pending_order,
			'total_order' => $total_order,
			'users' => User::count(),
			'recent_service' => $recent_service
		]);
	}

	protected function branch2() {
		$paid_service = Services::where('payment_status', '=', 'Paid')->count();

		$unpaid_service = Services::where('type', '=', 'Appointment')
			->where('payment_status', '=', 'Pending')->count();

		$pending_service = Services::where('type', '=', 'Appointment')
			->where('status', '<>', 'Cancelled')
			->where('status', '<>', 'Delivered')
			->count();

		$total_service = Services::where('status', '<>', 'Cancelled')
			->where('type', '=', 'Appointment')
			->where('branch', '=', 'branch2')->count();

		$recent_service = Services::where('type', '=', 'Appointment')
			->where('branch', '=', 'branch2')
			->orderBy('ID', 'DESC')->take(5)->get();

		return view ('admin.dashboard.branch2', [
			'paid_service' => $paid_service,
			'unpaid_service' => $unpaid_service,
			'pending_service' => $pending_service,
			'total_service' => $total_service,
			'users' => User::count(),
			'recent_service' => $recent_service
		]);


	}
}
