<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Products;
use App\ProductBrands;
use App\ProductComponent;
use App\ProductType;

use Auth;
use DB;
use Exception;
use Validator;

class StoreController extends Controller
{
    protected function index() {
		$product = Products::get();
        return view ('admin.store.index', [
            'product' => $product
        ]);
    }

    protected function show($id) {
		$product = Products::find($id);
		$img = $product->img;
        $imgArray = preg_split('/\s/', $img);

		return view ('admin.store.show', [
            'product' => $product,
			'img1' => $imgArray[0],
			'img2' => $imgArray[1],
			'img3' => $imgArray[2],
		]);
	}

    protected function create() {
		return view ('admin.store.create', [
            'brand' => ProductBrands::get(),
            'component' => ProductComponent::get(),
            'type' => ProductType::get()
        ]);
	}

	protected function store(Request $req) {

		$validator = Validator::make($req->all(), [
			'name' => 'required',
			'price' => 'required',
			'description' => 'required',
			'product_brand_id' => 'required',
			'product_type_id' => 'required',
			'product_component_id' => 'required',
			'category' => 'required',
            'img1' => 'image',
            'img2' => 'image',
            'img3' => 'image',
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
		
		try {
			DB::beginTransaction();

            $img1 = null;
            $img2 = null;
            $img3 = null;
            if ($req->hasFile('img1')) {
				$img1 = uniqid().'.'.$req->file('img1')->getClientOriginalExtension();
				$req->file('img1')->move('uploads/products/', $img1);
			}

            if ($req->hasFile('img2')) {
				$img2 = uniqid().'.'.$req->file('img2')->getClientOriginalExtension();
				$req->file('img2')->move('uploads/products/', $img2);
			}

            if ($req->hasFile('img3')) {
				$img3 = uniqid().'.'.$req->file('img3')->getClientOriginalExtension();
				$req->file('img3')->move('uploads/products/', $img3);
			}

            $img = $img1 . " " . $img2 . " " . $img3;

			$product = Products::create([
				'product_name' => $req->name,
				'price' => $req->price,
				'description' => $req->description,
				'product_brand_id' => $req->product_brand_id,
				'product_type_id' => $req->product_type_id,
				'product_component_id' => $req->product_component_id,
				'category' => $req->category,
                'img' => $img
			]);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			dd($e);
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('store.index')
			->with('flash_success', 'Successfully created new product.');
	}

	protected function edit($id) {
		$product = Products::find($id);
		$img = $product->img;
        $imgArray = preg_split('/\s/', $img);

		return view ('admin.store.edit', [
			'product' => $product,
			'img1' => $imgArray[0],
			'img2' => $imgArray[1],
			'img3' => $imgArray[2],
			'brand' => ProductBrands::get(),
            'component' => ProductComponent::get(),
            'type' => ProductType::get()
		]);
	}

	protected function update(Request $req, $id) {
		$product = Products::find($id);

		$validator = Validator::make($req->all(), [
			'name' => 'required',
			'price' => 'required',
			'description' => 'required',
			'product_brand_id' => 'required',
			'product_type_id' => 'required',
			'product_component_id' => 'required',
			'category' => 'required',
            'img1' => 'image',
            'img2' => 'image',
            'img3' => 'image',
		]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
                
		try {
			DB::beginTransaction();

            $img1 = null;
            $img2 = null;
            $img3 = null;

            if ($req->hasFile('img1')||$req->hasFile('img2')||$req->hasFile('img3')) {
				if ($product->img != null) {
                    $img = $product->img;
                    $imgArray = preg_split('/\s/', $img);

                    if ($req->hasFile('img1') != null) {
                        File::delete(public_path() . '/uploads/products/' . $imgArray[0]);

                        $destination = 'uploads/products';
                        $fileType = $req->file('img1')->getClientOriginalExtension();
                        $img1 = "image-" . uniqid() . "." . $fileType;
                        $req->file('img1')->move($destination, $img1);

                    } else { 
                        $img1 = $imgArray[0];
                    }

                    if ($req->hasFile('img2') != null) {
                        File::delete(public_path() . '/uploads/products/' . $imgArray[1]);

                        $destination = 'uploads/products';
                        $fileType = $req->file('img2')->getClientOriginalExtension();
                        $img2 = "image-" . uniqid() . "." . $fileType;
                        $req->file('img2')->move($destination, $img2);

                    } else { 
                        $img2 = $imgArray[1];
                    }

                    if ($req->hasFile('img3') != null) {
                        File::delete(public_path() . '/uploads/products/' . $imgArray[2]);

                        $destination = 'uploads/products';
                        $fileType = $req->file('img3')->getClientOriginalExtension();
                        $img1 = "image-" . uniqid() . "." . $fileType;
                        $req->file('img3')->move($destination, $img3);

                    } else { 
                        $img3 = $imgArray[2];
                    }
				}

                $img = $img1 . " " . $img2 . " " . $img3;
                $product->img = $img;
			}

			$product->product_name = $req->name;
			$product->price = $req->price;
			$product->description = $req->description;
			$product->product_brand_id = $req->product_brand_id;
            $product->product_type_id = $req->product_type_id;
			$product->product_component_id = $req->product_component_id;
			$product->category = $req->category;

			$product->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			dd($e);

			return redirect()
				->route('store.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('store.index')
			->with('flash_success', 'Successfully updated product.');
	}

    protected function delete($id) {
		try {
			DB::beginTransaction();
			$product = Products::find($id);
			$img = $product->img;
            $imgArray = preg_split('/\s/', $img);

			$product->delete();

            foreach ($imgArray as $i) {
                File::delete(public_path() . '/uploads/products/' . $i);
            }

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			dd($e);
			return redirect()
				->route('store.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('store.index')
			->with('flash_success', 'Successfully deleted product.');
	}
}
