<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\ShippingAddress;

use Auth;
use DB;
use Exception;
use Validator;

class AddressController extends Controller
{
    protected function index() {
		return view ('address.index');
	}

	protected function create() {
		return view ('address.create');
	}

    protected function edit($id) {
		$address = ShippingAddress::find($id);
		return view ('address.edit', [
			'address' => $address
		]);
	}

	protected function view($id) {
		$address = ShippingAddress::find($id);
		return view ('address.view', [
			'address' => $address
		]);
	}

	// AJAX RELATED
	public function getCities(Request $req) {
		$region = isset($req->region) ? $req->region : 'NCR';
		return ShippingAddress::getCities($region);
	}

	protected function updateAddress(Request $req, $id) {
		$user_id = Auth::User()->id;
		$address = ShippingAddress::find($id);

		$validator = Validator::make($req->all(), [
			'type' => 'required',
			'region' => 'required',
			'city' => 'required',
			'barangay' => 'required|min:2',
			'address' => 'required|min:2',
			]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		try {
			DB::beginTransaction();

			$address->type = $req->type;
			$address->region = $req->region;
			$address->city = $req->city;
			$address->barangay = $req->barangay;
			$address->address = $req->address;

			$address->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('address_checkout', [$user_id])
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('address_checkout', [$user_id])
			->with('flash_success', 'Successfully updated address.');
	}

	protected function storeAddress(Request $req) {
		$user_id = Auth::User()->id;

		$validator = Validator::make($req->all(), [
			'type' => 'required',
			'region' => 'required',
			'city' => 'required',
			'barangay' => 'required|min:2',
			'address' => 'required|min:2',
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
		
		try {
			DB::beginTransaction();

			$address = ShippingAddress::create([
				'user_id' => $user_id,
				'type' => $req->type,
				'region' => $req->region,
				'city' => $req->city,
				'barangay' => $req->barangay,
				'address' => $req->address,
				'is_selected' => '0'
			]);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			dd($e);

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('address_checkout',[$req->user_id])
			->with('flash_success', 'Successfully added shipping address.');
	}

	protected function deleteAddress($id) {
		$user_id = Auth::User()->id;

		try {
			DB::beginTransaction();
			$address = ShippingAddress::find($id);
			$address->delete();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			return redirect()
				->route('address_edit',[$user_id])
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('address_checkout', [$user_id])
			->with('flash_success', 'Successfully deleted address.');
	}



	protected function activeAddress($id) {
		$user_id = Auth::User()->id;
		$address = ShippingAddress::where('user_id','=', $user_id)->get();

		try {
			DB::beginTransaction();
			
			foreach ($address as $a) {
				$a->is_selected = 0;
				$a->save();
			}

			$a = ShippingAddress::find($id);

			if ($a == null) {
				return redirect()
					->back()
					->with('flash_info', 'Video already doesn\'t exists or was already removed.');
			}

			$a->is_selected = 1;
			$a->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			dd($e);
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully set an active address.');
	}

}
