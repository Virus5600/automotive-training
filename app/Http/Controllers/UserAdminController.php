<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\ShippingAddress;

use DB;
use Exception;
use Hash;
use Log;
use Validator;

class UserAdminController extends Controller
{
    protected function index() {
        return view ('admin.users.index', [
			'users' => User::get()
		]);
    }

	protected function show($id) {
        return view ('admin.users.show', [
			'users' => User::find($id),
            'address' => ShippingAddress::where('user_id','=',$id)->get()
		]);
    }

	protected function create() {
        return view ('admin.users.create');
    }

	protected function store(Request $req) {

		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'email' => 'required|email',
			'contact_no' => 'required|numeric|min:10',
			'type' => 'required',
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
		
		try {
			DB::beginTransaction();

			$user = User::create([
				'first_name' => $req->first_name,
				'last_name' => $req->last_name,
				'email' => $req->email,
				'contact_no' => $req->contact_no,
				'type' => $req->type,
				'password' => Hash::make('password'),
			]);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			
			return redirect()
				->route('user.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('user.index')
			->with('flash_success', 'Successfully added user default password is "password".');
	}

    protected function edit($id) {

		$users = User::find($id);

		if ($users == null) {
			return redirect()
				->back()
				->with('flash_info', 'User already doesn\'t exists or was already removed.');
		}

		return view('admin.users.edit', [
			'users' => $users,
            'address' => ShippingAddress::where('user_id','=',$id)->get()
		]);
	}

	protected function update(Request $req, $id) {
		$users = User::find($id);

		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'email' => 'required|email',
			'contact_no' => 'required|numeric|min:10',
			'type' => 'required',
			// 'avatar' => 'max:5120',
			]);
			// dd($validator);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');


		$imgValidator = null;

		$hasValidationProblem = $validator->fails();

			$imgValidator = Validator::make($req->all(), [
				'avatar' => 'mimes:jpeg,png,jpg,gif,svg|max:5120'
			], [
				'avatar.mimes' => 'Selected file doesn\'t match the allowed image formats.',
			]);

			// Now, if it does not match any of the formats, return with the merged error messages from $validator and $imgValidator.
			if ($imgValidator->fails() || $hasValidationProblem) {
				$validator->messages()->merge($imgValidator->messages());

				if ($req->hasFile('avatar')) {
					return redirect()
						->back()
						->withInput()
						->withErrors($validator)
						->with('flash_info', 'Please re-pick the image for your Avatar.')
						->with('trigger', true);
				}
				else {
					return redirect()
						->back()
						->withInput()
						->withErrors($validator->messages()->merge($imgValidator->messages()))
						->with('trigger', true);
				}
			}

			if ($hasValidationProblem) {
				if ($req->hasFile('avatar'))
					return redirect()
						->back()
						->withInput()
						->withErrors($validator)
						->with('flash_info', 'Please re-pick the image for your Avatar.')
						->with('trigger', true);
						
				return redirect()
					->back()
					->withInput()
					->withErrors($validator)
					->with('trigger', true);
			}

		$avatar = null;
		try {
			DB::beginTransaction();

			if ($req->hasFile('avatar')) {
				if ($users->avatar != null) {
				File::delete(public_path() . '/uploads/users/' . $users->avatar);
				}

				$destination = 'uploads/users';
				$fileType = $req->file('avatar')->getClientOriginalExtension();
				$avatar = "avatar-" . uniqid() . "." . $fileType;
				$req->file('avatar')->move($destination, $avatar);

				$users->avatar = $avatar;
			}

            $users->first_name = $req->first_name;
			$users->last_name = $req->last_name;
			$users->email = $req->email;
			$users->contact_no = $req->contact_no;
			$users->type = $req->type;
			$users->save();
			
			DB::commit();
		} catch (\Exception $e) {
			// dd($e);
			File::delete(public_path() . '/uploads/users/' . $avatar);
			DB::rollback();
			
			return redirect()
				->route('user.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('user.index')
			->with('flash_success', 'Successfully updated user "' . $users->id . '".');
	}

	public function changePassword($id) {
		return view('admin.users.edit-password',[
			'users' => User::find($id),
		]);
	}

	protected function updatePassword(Request $req, $id) {
		$users =  User::find($id);

		$validator = Validator::make($req->all(), [
			'old_password' => 'required|password_not_match:'.$users->password,
			'res_password' => 'required|same:res_password_confirmation|confirmed|different:old_password',
			'res_password_confirmation' => 'required|same:res_password'
		], [
			'old_password.password_not_match' => 'password not match'
		]);

		// dd($validator);

		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');

		try {
			DB::beginTransaction();

			$users->password = Hash::make($req->res_password);
			$users->save();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->route('user.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		
		return redirect()
			->route('user.index')
			->with('flash_success', 'Successfully updated password.');
	}

	protected function delete($id) {
		try {
			DB::beginTransaction();
			$users = User::find($id);
			$avatar = $users->avatar;
			DB::table('shipping_addresses')->where('user_id', '=', $id)->delete();
			File::delete(public_path() . '/uploads/users/' . $avatar);
			$users->delete();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			dd($e);
			return redirect()
				->route('user.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('user.index')
			->with('flash_success', 'Successfully deleted user.');
	}

	// SHIPPING ADDRESS

	
	protected function createAddress($id) {
        return view ('admin.users.create-address',[
			'users' => User::find($id)
		]);
    }

	protected function storeAddress(Request $req) {

		$validator = Validator::make($req->all(), [
			'user_id' => 'required',
			'type' => 'required',
			'region' => 'required',
			'city' => 'required',
			'barangay' => 'required|min:2',
			'address' => 'required|min:2',
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
		
		try {
			DB::beginTransaction();

			$address = ShippingAddress::create([
				'user_id' => $req->user_id,
				'type' => $req->type,
				'region' => $req->region,
				'city' => $req->city,
				'barangay' => $req->barangay,
				'address' => $req->address,
			]);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('user.edit',[$req->user_id])
			->with('flash_success', 'Successfully added shipping address.');
	}

	protected function deleteAddress($id, $user_id) {
		try {
			DB::beginTransaction();
			$address = ShippingAddress::find($id);
			$address->delete();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			return redirect()
				->route('user.edit',[$user_id])
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('user.edit',[$user_id])
			->with('flash_success', 'Successfully deleted address.');
	}

	protected function editAddress($id, $user_id) {

		$users = User::find($user_id);
		$address =  ShippingAddress::where('user_id','=',$user_id)
			->where('id','=',$id)
			->get()->first();

		if ($users == null) {
			return redirect()
				->back()
				->with('flash_info', 'User already doesn\'t exists or was already removed.');
		}

		return view('admin.users.edit-address', [
			'users' => $users,
            'address' => $address,
		]);
	}

	protected function updateAddress(Request $req, $id, $user_id) {
		$address = ShippingAddress::find($id);

		$validator = Validator::make($req->all(), [
			'type' => 'required',
			'region' => 'required',
			'city' => 'required',
			'barangay' => 'required|min:2',
			'address' => 'required|min:2',
			]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		try {
			DB::beginTransaction();

			$address->type = $req->type;
			$address->region = $req->region;
			$address->city = $req->city;
			$address->barangay = $req->barangay;
			$address->address = $req->address;

			$address->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);
			return redirect()
				->route('user.edit',[$user_id])
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('user.edit',[$user_id])
			->with('flash_success', 'Successfully updated address.');
	}
}