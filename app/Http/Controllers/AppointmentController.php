<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Services;
use App\ShippingAddress;

use Auth;
use DB;
use Exception;
use Validator;

class AppointmentController extends Controller
{
    protected function index() {
		$appointment = Services::where('email', '=', Auth::user()->email)
		->where('type', '=', 'Appointment')
		->orderBy('ID', 'DESC')->get();

		return view ('appointment.index', [
			'appointment' => $appointment
		]);
	}

	protected function create() {
		$id = Auth::user()->id;
		$address = ShippingAddress::where('user_id','=', $id)->get();

		return view ('appointment.create', [
			'address' => $address
		]);
	}

	protected function view($id) {
		$appointment = Services::find($id);

		return view ('appointment.view', [
			'appointment' => $appointment
		]);
	}

	protected function storeAppointment(Request $req) {

		$validator = Validator::make($req->all(), [
			'branch' => 'required',
			'contact_person' => 'required',
			'contact_no' => 'required',
			'address' => 'required',
			'subject_title' => 'required|min:5',
			'concern' => 'required|min:10',
			'appointment_date' => 'required|date|date_format:Y-m-d',
		]);



		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
		
		try {
			DB::beginTransaction();

			$appointment = Services::create([
				'contact_no' => $req->contact_no,
				'type' => 'Appointment',
				'email' => Auth::user()->email,
				'subject_title' => $req->subject_title,
				'address' => $req->address,
				'contact_person' => $req->contact_person,
				'branch' => $req->branch,
				'appointment_date' => $req->appointment_date,
				'concern' => $req->concern,
			]);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('appointment')
			->with('flash_success', 'Successfully booked appointment.');
	}


	// ADMIN SIDE

	protected function indexAdmin() {
		$appointment = Services::where('email', '=', Auth::user()->email)
		->where('type', '=', 'Appointment')
		->orderBy('ID', 'DESC')->get();

		return view ('admin.appointment.index', [
			'appointment' => $appointment
		]);
	}

	protected function viewAdmin($id) {
		$appointment = Services::find($id);

		return view ('admin.appointment.view', [
			'appointment' => $appointment
		]);
	}

	protected function editAdmin($id) {
		$appointment = Services::find($id);

		return view ('admin.appointment.edit', [
			'appointment' => $appointment
		]);
	}

	protected function updateAdmin(Request $req, $id) {
		$appointment = Services::find($id);

		$validator = Validator::make($req->all(), [
			'price' => 'required',
			'payment_status' => 'required',
			'status' => 'required',
			'admin_remarks' => 'required|min:2',
			]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		try {
			DB::beginTransaction();

			$appointment->price = $req->price;
			$appointment->payment_status = $req->payment_status;
			$appointment->status = $req->status;
			$appointment->admin_remarks = $req->admin_remarks;

			$appointment->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('appointment.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('appointment.index')
			->with('flash_success', 'Successfully updated appointment.');
	}

}
