<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\CarBrands;
use App\PaymentMethod;
use App\ProductBrands;
use App\ProductComponent;
use App\ProductType;

use Auth;
use DB;
use Exception;
use Hash;
use Validator;


class OthersController extends Controller
{
    protected function index() {
        return view ('admin.others.index');
    }

    protected function carindex() {
        return view ('admin.others.carindex',[
            'cars' => CarBrands::get()
        ]);
    }

    protected function carshow($id) {
        return view ('admin.others.carshow',[
            'car' => CarBrands::find($id)
        ]);
    }

    protected function carcreate() {
        return view ('admin.others.carcreate',[
            'car' => CarBrands::get()
        ]);
    }

    protected function carstore(Request $req) {
    
        $validator = Validator::make($req->all(), [
            'name' => 'required|min:2',
            'image' => 'image|required'
        ]);

        if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
        
        $image = null;
        try {
            DB::beginTransaction();

			if ($req->hasFile('image')) {
				$image = uniqid().'.'.$req->file('image')->getClientOriginalExtension();
				$req->file('image')->move('uploads/carbrands/', $image);
			}
    
            $car = CarBrands::create([
                'brand' => $req->name,
                'image' => $image,
            ]);
    
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            
            return redirect()
                ->back()
                ->with('flash_error', 'Something went wrong, please try again later.');
        }
    
        return redirect()
            ->route('car.index')
            ->with('flash_success', 'Car Brands Created Successfully.');
    }

    protected function caredit($id) {
        return view ('admin.others.caredit',[
            'car' => CarBrands::find($id)
        ]);
    }
    
	protected function carupdate(Request $req, $id) {
		$car = CarBrands::find($id);

		$validator = Validator::make($req->all(), [
			'name' => 'required|min:2',
			'image' => 'image',
			]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		$image = null;
		try {
			DB::beginTransaction();

			if ($req->hasFile('image')) {
				if ($car->image != null) {
				    File::delete(public_path() . '/uploads/carbrands/' . $car->image);
				}

				$destination = 'uploads/carbrands';
				$fileType = $req->file('image')->getClientOriginalExtension();
				$image = "image-" . uniqid() . "." . $fileType;
				$req->file('image')->move($destination, $image);

				$car->image = $image;
			}

            $car->brand = $req->name;
			$car->save();
			
			DB::commit();
		} catch (\Exception $e) {
			File::delete(public_path() . '/uploads/carbrands/' . $image);
			DB::rollback();
			
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Car Brand Updated Successfully.');
	}

    protected function cardelete($id) {
		try {
			DB::beginTransaction();
			$car = CarBrands::find($id);
            $image = $car->image;
            $name = $car->brand;
			$car->delete();
			File::delete(public_path() . '/uploads/carbrands/' . $image);

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();

			return redirect()
				->route('car.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('car.index')
			->with('flash_success', 'Successfully deleted "' . $name . '."');
	}

	// PRODUCT BRANDS
	protected function productBrand_index() {
        return view ('admin.others.productBrand_index',[
            'pbrand' => ProductBrands::get()
        ]);
    }

    protected function productBrand_create() {
        return view ('admin.others.productBrand_create',[
            'pbrand' => ProductBrands::get()
        ]);
    }

    protected function productBrand_store(Request $req) {
    
        $validator = Validator::make($req->all(), [
            'name' => 'required|min:2',
        ]);

        if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
        
        try {
            DB::beginTransaction();
    
            $pbrand = ProductBrands::create([
                'name' => $req->name,
            ]);
    
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            
            return redirect()
                ->back()
                ->with('flash_error', 'Something went wrong, please try again later.');
        }
    
        return redirect()
            ->route('pBrand.index')
            ->with('flash_success', 'Product Brand Created Successfully.');
    }

	protected function productBrand_delete($id) {
		try {
			DB::beginTransaction();
			$pBrand = ProductBrands::find($id);
            $name = $pBrand->name;
			$pBrand->delete();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();

			return redirect()
				->route('pBrand.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('pBrand.index')
			->with('flash_success', 'Successfully deleted "' . $name . '."');
	}

	// PRODUCT TYPE

	protected function productType_index() {
        return view ('admin.others.productType_index',[
            'type' => ProductType::get()
        ]);
    }

    protected function productType_create() {
        return view ('admin.others.productType_create',[
            'type' => ProductType::get()
        ]);
    }

    protected function productType_store(Request $req) {
    
        $validator = Validator::make($req->all(), [
            'name' => 'required|min:2',
            'category' => 'required'
        ]);

        if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
        
        try {
            DB::beginTransaction();

            $type = ProductType::create([
                'name' => $req->name,
                'category' => $req->category,
            ]);
    
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            
            return redirect()
                ->back()
                ->with('flash_error', 'Something went wrong, please try again later.');
        }
    
        return redirect()
            ->route('pType.index')
            ->with('flash_success', 'Product Type Created Successfully.');
    }

    protected function productType_edit($id) {
        return view ('admin.others.productType_edit',[
            'type' => ProductType::find($id)
        ]);
    }
    
	protected function productType_update(Request $req, $id) {
		$type = ProductType::find($id);

		$validator = Validator::make($req->all(), [
			'name' => 'required|min:2',
			'category' => 'required',
			]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		try {
			DB::beginTransaction();

            $type->name = $req->name;
			$type->category = $req->category;
			$type->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('pType.index')
			->with('flash_success', 'Product Type Updated Successfully.');
	}

    protected function productType_delete($id) {
		try {
			DB::beginTransaction();
			$type = ProductType::find($id);
            $name = $type->name;
			$type->delete();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();

			return redirect()
				->route('pType.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('pType.index')
			->with('flash_success', 'Successfully deleted "' . $name . '."');
	}

    // PAYMENT METHOD

	protected function payment_index() {
        return view ('admin.others.payment_index',[
            'payment' => PaymentMethod::get()
        ]);
    }

    protected function payment_show($id) {
        return view ('admin.others.payment_show',[
            'payment' => PaymentMethod::find($id)
        ]);
    }

    protected function payment_create() {
        return view ('admin.others.payment_create');
    }

    protected function payment_store(Request $req) {
    
        $validator = Validator::make($req->all(), [
            'name' => 'required|min:2',
            'info' => 'required',
            'method' => 'required',
            'image' => 'image|required'
        ]);

        if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
               
        $image = null;
        try {
            DB::beginTransaction();

            if ($req->hasFile('image')) {
                $image = uniqid().'.'.$req->file('image')->getClientOriginalExtension();
                $req->file('image')->move('uploads/method/', $image);
            }

            $payment = PaymentMethod::create([
                'name' => $req->name,
                'info' => $req->info,
                'method' => $req->method,
                'logo' => $image,
            ]);
    
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            return redirect()
                ->back()
                ->with('flash_error', 'Something went wrong, please try again later.');
        }
    
        return redirect()
            ->route('payment.index')
            ->with('flash_success', 'Payment Method Created Successfully.');
    }

    protected function payment_edit($id) {
        return view ('admin.others.payment_edit',[
            'payment' => PaymentMethod::find($id)
        ]);
    }
    
	protected function payment_update(Request $req, $id) {
		$payment = PaymentMethod::find($id);

		$validator = Validator::make($req->all(), [
			'name' => 'required|min:2',
            'info' => 'required',
            'method' => 'required',
			'image' => 'image',
        ]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

        $image = null;
		try {
			DB::beginTransaction();

			if ($req->hasFile('image')) {
				if ($payment->logo != null) {
				    File::delete(public_path() . '/uploads/method/' . $payment->logo);
				}

				$destination = 'uploads/method';
				$fileType = $req->file('image')->getClientOriginalExtension();
				$image = "image-" . uniqid() . "." . $fileType;
				$req->file('image')->move($destination, $image);

				$payment->logo = $image;
			}
			DB::beginTransaction();

            $payment->name = $req->name;
			$payment->info = $req->info;
			$payment->method = $req->method;
			$payment->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('payment.index')
			->with('flash_success', 'Payment Method Updated Successfully.');
	}

    protected function payment_delete($id) {
		try {
			DB::beginTransaction();

			$payment = PaymentMethod::find($id);
            $image = $payment->logo;
			$payment->delete();

			File::delete(public_path() . '/uploads/method/' . $image);

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();

			return redirect()
				->route('payment.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('payment.index')
			->with('flash_success', 'Successfully deleted payment method.');
	}

    // PRODUCT COMPONENT

    protected function component_index() {
        return view ('admin.others.component_index',[
            'component' => ProductComponent::get()
        ]);
    }

    protected function component_create() {
        return view ('admin.others.component_create',[
            'pType' => ProductType::get()
        ]);
    }

    protected function component_store(Request $req) {
    
        $validator = Validator::make($req->all(), [
            'name' => 'required|min:2',
            'product_type' => 'required'
        ]);

        if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
        
        try {
            DB::beginTransaction();
    
            $pComponent = ProductComponent::create([
                'name' => $req->name,
                'product_type_id' => $req->product_type
            ]);
    
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            
            return redirect()
                ->back()
                ->with('flash_error', 'Something went wrong, please try again later.');
        }
    
        return redirect()
            ->route('component.index')
            ->with('flash_success', 'Product Component Created Successfully.');
    }

    protected function component_edit($id) {
        return view ('admin.others.component_edit',[
            'component' => ProductComponent::find($id),
            'pType' => ProductType::get()
        ]);
    }
    
	protected function component_update(Request $req, $id) {
		$component = ProductComponent::find($id);

		$validator = Validator::make($req->all(), [
            'name' => 'required|min:2',
            'product_type' => 'required'
        ]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		try {
			DB::beginTransaction();

            $component->name = $req->name;
			$component->product_type_id = $req->product_type;
			$component->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('component.index')
			->with('flash_success', 'Product Component Updated Successfully.');
	}

	protected function component_delete($id) {
		try {
			DB::beginTransaction();
			$component = ProductComponent::find($id);
			$component->delete();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();

			return redirect()
				->route('component.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('component.index')
			->with('flash_success', 'Successfully deleted product component.');
	}
}
