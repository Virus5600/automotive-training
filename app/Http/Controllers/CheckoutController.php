<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CheckoutController extends Controller
{
    protected function index() {
        return view ('checkout.index');
    }

    protected function view() {
        return view ('checkout.view');
    }

    protected function pay() {
        return view ('checkout.pay');
    }
}
