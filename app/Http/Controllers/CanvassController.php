<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Services;

use Auth;
use DB;
use Exception;
use Validator;


class CanvassController extends Controller
{
    protected function index() {
		$canvass = Services::where('email', '=', Auth::user()->email)
		->where('type', '=', 'Canvass')
		->orderBy('ID', 'DESC')->get();

		return view ('canvass.index', [
			'canvass' => $canvass
		]);
	}

    protected function create() {
		return view ('canvass.create');
	}

	protected function view($id) {
		$canvass = Services::find($id);

		return view ('canvass.view', [
			'canvass' => $canvass
		]);
	}

	protected function storeCanvass(Request $req) {

		$validator = Validator::make($req->all(), [
			'contact_person' => 'required',
			'contact_no' => 'required',
			'subject_title' => 'required|min:5',
			'concern' => 'required|min:10',
		]);


		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
		
		try {
			DB::beginTransaction();

			$canvass = Services::create([
				'contact_no' => $req->contact_no,
				'type' => 'Canvass',
				'email' => Auth::user()->email,
				'subject_title' => $req->subject_title,
				'contact_person' => $req->contact_person,
				'concern' => $req->concern,
			]);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('canvass')
			->with('flash_success', 'Successfully submitted canvassing.');
	}

    
	// ADMIN SIDE

	protected function indexAdmin() {
		$canvass = Services::where('email', '=', Auth::user()->email)
		->where('type', '=', 'Canvass')
		->orderBy('ID', 'DESC')->get();

		return view ('admin.canvass.index', [
			'canvass' => $canvass
		]);
	}

	protected function viewAdmin($id) {
		$canvass = Services::find($id);

		return view ('admin.canvass.view', [
			'canvass' => $canvass
		]);
	}

	protected function editAdmin($id) {
		$canvass = Services::find($id);

		return view ('admin.canvass.edit', [
			'canvass' => $canvass
		]);
	}

	protected function updateAdmin(Request $req, $id) {
		$canvass = Services::find($id);

		$validator = Validator::make($req->all(), [
			'admin_remarks' => 'required|min:2',
			]);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');

		try {
			DB::beginTransaction();

			$canvass->admin_remarks = $req->admin_remarks;

			$canvass->save();
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('canvass.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('canvass.index')
			->with('flash_success', 'Successfully updated canvass.');
	}
}
