<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\CarBrands;
use App\PaymentMethod;
use App\Products;
use App\ProductBrands;
use App\ProductComponent;
use App\ProductType;
use App\ShippingAddress;

use Auth;
use DB;
use Exception;
use Hash;
use Validator;

class UserSideController extends Controller
{

    protected function home() {
		$product = Products::get();

		return view ('home',[
			'car' => CarBrands::get(),
			'product' => $product,

		]);
	}

    protected function products() {
		return view ('products',[
			'pBrand' => ProductBrands::get(),
            'car' => ProductType::where('category','=','Car')->get(),
            'motor' => ProductType::where('category','=','Motor')->get(),
            'others' => ProductType::where('category','=','Others')->get(),
			'component' => ProductComponent::get(),
			'product' => Products::get(),
		]);
	}

    protected function about_us() {
		return view ('about_us');
	}

    protected function payment_methods() {
		return view ('payment_methods', [
			'payment' => PaymentMethod::get()
		]);
	}

    protected function contact_us() {
		return view ('contact_us');
	}

	protected function login() {
		return view ('login');
	}

	protected function register() {
		return view ('register');
	}

	protected function profile() {
        $id = Auth::user()->id;
		$address = ShippingAddress::where('user_id','=', $id)->get();

		return view ('profile.view',[
            'users' => User::find($id),
			'address' => $address
        ]);
	}

	protected function edit_profile() {
		$id = Auth::user()->id;

		return view ('profile.edit', [
			'users' => User::find($id),
		]);
	}

	protected function wishlist() {
		return view ('wishlist');
	}

	protected function address_checkout() {
		$id = Auth::user()->id;
		$address = ShippingAddress::where('user_id','=', $id)->get();

		return view ('address.index', [
			'address' => $address
		]);
	}

	protected function reset_password() {
		return view ('reset_password');
	}

	protected function settings_profile() {
		return view ('settings.edit_profile', [
			'users' => Auth::user()
		]);
	}

	protected function settings_password() {
		return view ('settings.edit_password');
	}

	protected function settings_email() {
		return view ('settings.edit_email');
	}

	protected function myorder() {
		return view ('myorder');
	}

	protected function track_order() {
		return view ('track_order');
	}

	protected function mycart() {
		$id = Auth::user()->id;
		$address = ShippingAddress::where('user_id','=', $id)->get();

		return view ('my_cart', [
			'address' => $address,
			'payment' => PaymentMethod::get()
		]);
	}

	protected function details($id) {
		$product = Products::find($id);
		$img = $product->img;
        $imgArray = preg_split('/\s/', $img);

		return view ('view_details', [
			'product' => $product,
			'img1' => $imgArray[0],
			'img2' => $imgArray[1],
			'img3' => $imgArray[2],
		]);
	}

	protected function authenticate(Request $req) {
		$validator = Validator::make($req->all(), [
			'email' => 'required|email',
			'password' => 'required'
		]);

		if (Auth::attempt(['email' => $req->email, 'password' => $req->pwd])) {
			
			return redirect()->intended('/')->with('flash_success', 'Logged in!');
		}
		else {
			auth()->logout();
			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Wrong email/password!');
		}
	}

	protected function logout() {
		if (Auth::check()) {
			auth()->logout();
			return redirect()->intended('/')->with('flash_success', 'Logged out!');
		}
		return redirect()->back()->with('flash_error', 'Something went wrong, please try again.');
	}
    
    protected function store(Request $req) {
    
        $validator = Validator::make($req->all(), [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'email' => 'required|email',
            'password' => 'required|min:7',
            'conf_password' => 'required|same:password'
        ]);

        if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');
        
        try {
            DB::beginTransaction();
    
            $user = User::create([
                'first_name' => $req->first_name,
                'last_name' => $req->last_name,
                'email' => $req->email,
                'contact_no' => $req->contact_no,
                'type' => 'Client',
                'password' => Hash::make($req->password),
            ]);
    
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            
            return redirect()
                ->back()
                ->with('flash_error', 'Something went wrong, please try again later.');
        }
    
        return redirect()
            ->route('login')
            ->with('flash_success', 'Registered Successfully.');
    }
    
	protected function update(Request $req) {
		$users = Auth::user();

		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'contact_no' => 'required|min:10',
			// 'avatar' => 'max:5120',
			]);
			// dd($validator);

            if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Invalid input, please try again later.');


		$imgValidator = null;

		$hasValidationProblem = $validator->fails();

			$imgValidator = Validator::make($req->all(), [
				'avatar' => 'mimes:jpeg,png,jpg,gif,svg|max:5120'
			], [
				'avatar.mimes' => 'Selected file doesn\'t match the allowed image formats.',
			]);

			// Now, if it does not match any of the formats, return with the merged error messages from $validator and $imgValidator.
			if ($imgValidator->fails() || $hasValidationProblem) {
				$validator->messages()->merge($imgValidator->messages());

				if ($req->hasFile('avatar')) {
					return redirect()
						->back()
						->withInput()
						->withErrors($validator)
						->with('flash_info', 'Please re-pick the image for your Avatar.')
						->with('trigger', true);
				}
				else {
					return redirect()
						->back()
						->withInput()
						->withErrors($validator->messages()->merge($imgValidator->messages()))
						->with('trigger', true);
				}
			}

			if ($hasValidationProblem) {
				if ($req->hasFile('avatar'))
					return redirect()
						->back()
						->withInput()
						->withErrors($validator)
						->with('flash_info', 'Please re-pick the image for your Avatar.')
						->with('trigger', true);
						
				return redirect()
					->back()
					->withInput()
					->withErrors($validator)
					->with('trigger', true);
			}

		$avatar = null;
		try {
			DB::beginTransaction();

			if ($req->hasFile('avatar')) {
				if ($users->avatar != null) {
				File::delete(public_path() . '/uploads/users/' . $users->avatar);
				}

				$destination = 'uploads/users';
				$fileType = $req->file('avatar')->getClientOriginalExtension();
				$avatar = "avatar-" . uniqid() . "." . $fileType;
				$req->file('avatar')->move($destination, $avatar);

				$users->avatar = $avatar;
			}

            $users->first_name = $req->first_name;
			$users->last_name = $req->last_name;
			$users->contact_no = $req->contact_no;
			$users->save();
			
			DB::commit();
		} catch (\Exception $e) {
			dd($e);
			File::delete(public_path() . '/uploads/users/' . $avatar);
			DB::rollback();
			
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Profile Updated Successfully.');
	}

	protected function change_password(Request $req) {
		$users =  Auth::user();

		$validator = Validator::make($req->all(), [
			'old_password' => 'required|password_not_match:'.$users->password,
			'res_password' => 'required|same:res_password_confirmation|confirmed|different:old_password',
			'res_password_confirmation' => 'required|same:res_password'
		], [
			'old_password.password_not_match' => 'password not match'
		]);

		// dd($validator);

		if ($validator->fails())
			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');

		try {
			DB::beginTransaction();

			$users->password = Hash::make($req->res_password);
			$users->save();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		
		return redirect()
			->back()
			->with('flash_success', 'Successfully updated password.');
	}
    

}



