<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    protected $fillable = [
    	'user_id',
		'type',
		'region',
        'city',
		'barangay',
        'address',
		'is_selected',
	];

	// Relationship
	protected function user() { return $this->belongsTo('App\User'); }

	// Custom Functions
	public static function getRegions() {
		$values = array(
			'NCR',
			'CAR',
			'Region I',
			'Region II',
			'Region III',
			'Region IV-A',
			'Region IV-B',
			'Region V',
			'Region VI',
			'Region VII',
			'Region VIII',
			'Region IX',
			'Region X',
			'Region XI',
			'Region XII',
			'Region XIII',
			'BARMM',
		);
		return $values;
	}
	
	public static function getCities($region='NCR') {
		if ($region == null)
			$region = 'NCR';
	
		$cities = array(
			'NCR' => array('Caloocan', 'Las Piñas', 'Makati', 'Malabon', 'Mandaluyong', 'Manila', 'Marikina', 'Muntinlupa', 'Navotas', 'Parañaque', 'Pasay', 'Pasig', 'Pateros', 'Quezon City', 'San Juan', 'Taguig', 'Valenzuela'),
			'CAR' => array('Abra', 'Apayao', 'Baguio', 'Benguet', 'Ifugao', 'Kalinga', 'Mountain Province'),
			'Region I' => array('Dagupan', 'Ilocos Norte', 'Ilocos Sur', 'La Union', 'Pangasinan'),
			'Region II' => array('Batanes', 'Cagayan', 'Isabela', 'Nueva Vizcaya', 'Quirino', 'Santiago'),
			'Region III' => array('Angeles City', 'Aurora', 'Bataan', 'Bulacan', 'Nueva Ecija', 'Olongapo', 'Pampanga', 'Tarlac', 'Zambales'),
			'Region IV-A' => array('Batangas', 'Cavite', 'Laguna', 'Lucena', 'Quezon', 'Rizal'),
			'Region IV-B' => array('Marinduque', 'Occidental Mindoro', 'Oriental Mindoro', 'Palawan', 'Puerto Princesa', 'Romblon'),
			'Region V' => array('Albay', 'Camarines Norte', 'Camarines Sur', 'Catanduanes', 'Masbate', 'Naga', 'Sorsogon'),
			'Region VI' => array('Albay', 'Camarines Norte', 'Camarines Sur', 'Catanduanes', 'Masbate', 'Naga', 'Sorsogon'),
			'Region VII' => array('Bohol', 'Cebu', 'Cebu City', 'Lapu-Lapu', 'Mandaue', 'Negros Oriental', 'Siquijor'),
			'Region VIII' => array('Biliran', 'Eastern Samar', 'Leyte', 'Northern Samar', 'Ormoc', 'Samar', 'Southern Leyte', 'Tacloban'),
			'Region IX' => array('Isabela City', 'Zamboanga City', 'Zamboanga del Norte', 'Zamboanga del Sur', 'Zamboanga Sibugay'),
			'Region X' => array('Bukidnon', 'Cagayan de Oro', 'Camiguin', 'Iligan', 'Lanao del Norte', 'Misamis Occidental', 'Misamis Oriental'),
			'Region XI' => array('Davao City', 'Davao de Oro', 'Davao del Norte', 'Davao del Sur', 'Davao Occidental', 'Davao Oriental'),
			'Region XII' => array('Cotabato', 'General Santos', 'Sarangani', 'South Cotabato', 'Sultan Kudarat'),
			'Region XIII' => array('Agusan del Norte', 'Agusan del Sur', 'Butuan', 'Dinagat Islands', 'Surigao del Norte', 'Surigao del Sur'),
			'BARMM' => array('Basilan', 'Cotabato City', 'Lanao del Sur', 'Maguindanao', 'Sulu', 'Tawi-Tawi', '63 barangays in Cotabato')
		);
	
		return $cities[$region];
	}
}