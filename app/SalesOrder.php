<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    protected $fillable = [
        'user_id',
        'customer_email',
        'payment_status',
        'status',
        'total_price',
        'shipping_address',
        'delivered_date'
    ];
}
