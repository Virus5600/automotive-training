<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLists extends Model
{
    protected $fillable = [
    	'sales_order_id',
		'product_id',
        'quantity',
		'subtotal',
	];

	// Relationship
	protected function salesOrder() { return $this->belongsTo('App\SalesOrder'); }
	protected function product() { return $this->belongsTo('App\Products'); }

}
