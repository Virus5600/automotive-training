<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $fillable = [
    	'sales_order_id',
		'img',
	];

	// Relationship
	protected function salesOrder() { return $this->belongsTo('App\SalesOrder'); }

}
