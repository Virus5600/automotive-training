<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Hash;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend(
			'password_not_match',
			function ($attribute, $value, $parameters, $validator) {
				return Hash::check($value, $parameters[0]);
			}
		);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
