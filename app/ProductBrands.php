<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBrands extends Model
{
    protected $fillable = [
    	'name',
	];
}
