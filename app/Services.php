<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $fillable = [
    	'type',
    	'appointment_date',
		'branch',
        'contact_person',
        'contact_no',
        'email',
        'subject_title',
        'concern',
		'img',
        'admin_remarks',
		'payment_status',
        'status',
		'price',
        'address'
	];

}
