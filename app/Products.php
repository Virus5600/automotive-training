<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Products;
use App\ProductBrands;
use App\ProductType;
use App\ProductComponent;

class Products extends Model
{
    protected $fillable = [
    	'product_name',
		'price',
        'description',
		'product_brand_id',
        'product_type_id',
        'product_component_id',
		'category',
		'img'
	];

	// Relationship
	protected function brand() { return $this->belongsTo('App\ProductBrands'); }
	protected function type() { return $this->belongsTo('App\ProductType'); }
	protected function component() { return $this->belongsTo('App\ProductComponent'); }

	public function pbrand($id) { 
		return ProductBrands::find($id);
	}
	public function ptype($id) { 
		return ProductType::find($id);
	}
	public function pcomponent($id) { 
		return ProductComponent::find($id);
	}

	public function getImg($id) { 
		$product = Products::find($id);
		$img = $product->img;
        $imgArray = preg_split('/\s/', $img);

		if ($imgArray[0] != null) {
			return $imgArray[0];
		} elseif ($imgArray[1] != null){
			return $imgArray[1];
		} elseif ($imgArray[2] != null){
			return $imgArray[2];
		}
	}
}
