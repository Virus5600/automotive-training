<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
			'first_name' => 'Admin',
			'last_name' => 'Admin',
			'email' => 'admin@admin.com',
			'contact_no' => '9277916894',
			'type' => 'Admin',
			'password' => Hash::make('admin'),
        ]); //for testing

        User::create([
			'first_name' => 'Owner',
			'last_name' => 'Owner',
			'email' => 'owner@owner.com',
			'contact_no' => '9277916894',
			'type' => 'Owner',
			'password' => Hash::make('owner'),
        ]);

    }
}
