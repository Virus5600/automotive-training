<?php

use Illuminate\Database\Seeder;

use App\Settings;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create([
			'settings' => 'branch1',
			'value' => 'branch1_address',
        ]);
        Settings::create([
			'settings' => 'branch2',
			'value' => 'branch2_address',
        ]);
        Settings::create([
			'settings' => 'contact_no',
			'value' => '9277916894',
        ]);
        Settings::create([
			'settings' => 'facebook_link',
			'value' => 'facebook_link',
        ]);
        Settings::create([
			'settings' => 'logo',
			'value' => 'logo.png',
        ]);
        Settings::create([
			'settings' => 'tos',
			'value' => 'tos',
        ]);
        Settings::create([
			'settings' => 'about',
			'value' => 'about',
        ]);
        Settings::create([
			'settings' => 'about2',
			'value' => 'about2',
        ]);
        Settings::create([
			'settings' => 'privacy_policy',
			'value' => 'privacy_policy',
        ]);
    }
}
