<?php

use Illuminate\Database\Seeder;

use App\CarBrands;
use App\PaymentMethod;

class OtherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CarBrands::create([
			'brand' => 'Suzuki',
			'image' => 'br1.png',
		]);
        CarBrands::create([
			'brand' => 'Bajaj',
			'image' => 'br2.png',
		]);
        CarBrands::create([
			'brand' => 'Honda',
			'image' => 'br3.png',
		]);
        CarBrands::create([
			'brand' => 'Toyota',
			'image' => 'br4.png',
		]);
        CarBrands::create([
			'brand' => 'Mitsubishi',
			'image' => 'br5.png',
		]);
        CarBrands::create([
			'brand' => 'Kawasaki',
			'image' => 'br6.png',
		]);
        CarBrands::create([
			'brand' => 'Yamaha',
			'image' => 'br7.png',
		]);

		PaymentMethod::create([
			'name' => 'Jhasmin Borromeo',
			'method' => 'GCASH',
			'info' => '09277916894',
			'logo' => 'gcash.png'
		]);

		PaymentMethod::create([
			'name' => 'Jhasmin Borromeo',
			'method' => 'PAYPAL',
			'info' => 'paypal.me/jhaaaaasmin',
			'logo' => 'paypal.png'
		]);

		PaymentMethod::create([
			'name' => 'Jhasmin Borromeo',
			'method' => 'LANDBANK',
			'info' => '4567-9143-2678-3342',
			'logo' => 'landbank.png'
		]);
    }
}
