<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
			$table->enum('type', ['Canvass', 'Appointment']);
            $table->date('appointment_date')->nullable();
            $table->enum('branch', ['Branch1', 'Branch2'])->nullable();
            $table->string('contact_person');
            $table->string('contact_no');
            $table->integer('price')->nullable();
            $table->string('email');
            $table->string('subject_title');
            $table->string('concern');
            $table->string('img')->nullable();
            $table->string('address')->nullable();
            $table->string('admin_remarks')->nullable();
			$table->enum('payment_status', ['Pending', 'Paid'])->default('Pending');
			$table->enum('status', ['Cancelled', 'Processing', 'Confirmed', 'On The Way', 'Done'])->default('Processing');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
