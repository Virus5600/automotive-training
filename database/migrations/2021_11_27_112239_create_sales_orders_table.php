<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('customer_email');
			$table->enum('payment_status', ['Pending', 'Paid'])->default('Pending');
			$table->enum('status', ['Cancelled', 'Processing', 'Confirmed', 'Shipped', 'On The Way', 'Delivered'])->default('Processing');
            $table->integer('total_price');
            $table->string('shipping_address');
            $table->date('delivered_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_orders');
    }
}
