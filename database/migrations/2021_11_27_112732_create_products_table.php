<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_name');
            $table->integer('price');
            $table->mediumText('description');
            $table->integer('product_brand_id')->unsigned();
            $table->integer('product_type_id')->unsigned();
            $table->integer('product_component_id')->unsigned();
			$table->enum('category', ['All', 'Car', 'Motor', 'Others']);
			$table->mediumText('img');
            $table->timestamps();

            $table->foreign('product_brand_id')->references('id')->on('product_brands')->onDelete('cascade');
            $table->foreign('product_type_id')->references('id')->on('product_types')->onDelete('cascade');
            $table->foreign('product_component_id')->references('id')->on('product_components')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
